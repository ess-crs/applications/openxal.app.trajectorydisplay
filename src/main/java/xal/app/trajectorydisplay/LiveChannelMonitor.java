/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.trajectorydisplay;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import xal.ca.BatchConnectionRequest;
import xal.ca.Channel;
import xal.ca.ChannelRecord;
import xal.ca.ConnectionListener;
import xal.ca.IEventSinkValue;
import xal.ca.Monitor;
import xal.ca.MonitorException;
import xal.smf.impl.BPM;
import xal.smf.impl.DipoleCorr;

/**
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class LiveChannelMonitor extends AbstractMonitorsHandler implements IEventSinkValue, ConnectionListener {

    private List<Channel> channelList;
    private HashMap<Channel,BPM> inputChannelsX;
    private HashMap<Channel,BPM> inputChannelsY;
    private HashMap<Channel,BPM> inputChannelsAmpl;
    private HashMap<Channel,DipoleCorr> inputChannelsCV;
    private HashMap<Channel,DipoleCorr> inputChannelsCH;
    private final HashMap<Channel,Boolean> channelUpdate = new HashMap();
    private int syncTim = 100;

    LiveChannelMonitor(TrajectoryArray trajectory, CorrectorArray correctors, EventHandler<ActionEvent> handler) {
        super(trajectory, correctors, handler);        
                
    }    

    @Override
    public void initializeMonitors() {
        
        BatchConnectionRequest request = new BatchConnectionRequest(this.channelList);
        request.submitAndWait(5.0);
        
        this.channelList.forEach(chan->{
            chan.addConnectionListener(this);        
        });

    }

    @Override
    public void eventValue(ChannelRecord cr, Channel chnl) {       
        boolean newData = false;
        if (chnl.isConnected()) {
            //Platform.runLater(
            //() -> {
            if (inputChannelsX.keySet().contains(chnl)){
                this.trajectory.setPositionX(inputChannelsX.get(chnl),cr.doubleValue());
                if (this.trajectory.getPositionX(inputChannelsX.get(chnl)) != cr.doubleValue()) {
                    newData = true;
                }
            } else if (inputChannelsY.keySet().contains(chnl)){
                this.trajectory.setPositionY(inputChannelsY.get(chnl),cr.doubleValue());
                if (this.trajectory.getPositionX(inputChannelsY.get(chnl)) != cr.doubleValue()) {
                    newData = true;
                }
            } else if (inputChannelsAmpl.keySet().contains(chnl)){
                this.trajectory.setAmplitude(inputChannelsAmpl.get(chnl),cr.doubleValue());
                if (this.trajectory.getPositionX(inputChannelsAmpl.get(chnl)) != cr.doubleValue()) {
                    newData = true;
                }
            }else if (inputChannelsCV.keySet().contains(chnl)){
                this.correctors.setCV(inputChannelsCV.get(chnl), cr.doubleValue()); 
                if (this.correctors.getCV(inputChannelsCV.get(chnl)) != cr.doubleValue()) {
                    newData = true;
                }
            }else if (inputChannelsCH.keySet().contains(chnl)){
                this.correctors.setCH(inputChannelsCH.get(chnl), cr.doubleValue());
                if (this.correctors.getCH(inputChannelsCH.get(chnl)) != cr.doubleValue()) {
                    newData = true;
                }
            }               
            //});
            }
        if (newData) {
            notifyNewUpdate();
        }
    }

    @Override
    public void connectionMade(Channel chnl) {
        try {
            if (chnl.isConnected()) {
                Monitor monitor = chnl.addMonitorValue(this, 0);
                monitors.add(monitor);
            }
        } catch (MonitorException ex) {
            Logger.getLogger(LiveChannelMonitor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NullPointerException ex) {
            Logger.getLogger(LiveChannelMonitor.class.getName()).log(Level.WARNING, "Not possible to connect to channel {0}.", chnl.channelName());
            Logger.getLogger(LiveChannelMonitor.class.getName()).log(Level.FINE, "NullPointerException while connecting to channel " + chnl.channelName() + " ", ex);
            chnl.disconnect();
            chnl.requestConnection();
        }
    }

    @Override
    public void connectionDropped(Channel chnl) {
        Logger.getLogger(LiveChannelMonitor.class.getName()).log(Level.WARNING, "Channel {0} disconnected.", chnl.channelName());
    }
    
    
    public void checkUpdate(){
        if (!channelUpdate.values().contains(false)){
            channelUpdate.keySet().forEach(chan ->{
                if (inputChannelsCV.keySet().contains(chan) || inputChannelsCH.keySet().contains(chan)){
                    channelUpdate.put(chan,true);
                } else {
                    channelUpdate.put(chan,false);
                }
                
            });
            notifyNewUpdate();
        }
    }
    
    public boolean isAlive(){
        return !this.stopFlag;
    }
    
    public void updatechannels(HashMap<Channel,BPM> inputChannelsX ,HashMap<Channel,BPM> inputChannelsY ,HashMap<Channel,BPM> inputChannelsAmpl, HashMap<Channel,DipoleCorr> inputChannelsCV, 
            HashMap<Channel,DipoleCorr> inputChannelsCH){
        
        this.channelList = new ArrayList<>();
        channelList.addAll(inputChannelsX.keySet());
        channelList.addAll(inputChannelsY.keySet());
        channelList.addAll(inputChannelsAmpl.keySet());
        channelList.addAll(inputChannelsCV.keySet());
        channelList.addAll(inputChannelsCH.keySet());
        
        this.inputChannelsX = inputChannelsX;
        this.inputChannelsY = inputChannelsY;
        this.inputChannelsAmpl = inputChannelsAmpl;
        this.inputChannelsCV = inputChannelsCV;
        this.inputChannelsCH = inputChannelsCH;              
        
    }
    
    
}
