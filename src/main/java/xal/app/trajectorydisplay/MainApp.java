/*
 * Copyright (C) 2018 European Spallation Source ERIC
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.trajectorydisplay;

import static javafx.application.Application.launch;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;

import xal.extension.fxapplication.FxApplication;
import xal.tools.apputils.Preferences;

public class MainApp extends FxApplication {

    @Override
    public void setup(Stage stage) {

        MAIN_SCENE = "/fxml/TrajectoryDisplay.fxml";
        CSS_STYLE = "/styles/Styles.css";
        setApplicationName("Trajectory Display");
        HAS_DOCUMENTS = false;
        HAS_SEQUENCE = true;
        DOCUMENT = new TrajectoryDisplayDocument(stage);

        MainFunctions.initialize((TrajectoryDisplayDocument) DOCUMENT);

        //super.initialize();
        //super.start(stage);
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void beforeStart(Stage stage) {

        Menu trajectoryMenu = new Menu("Trajectory");

        
        MenuItem trajFromFileMenu = new MenuItem("Display trajectory from File");
        trajFromFileMenu.setOnAction(new TrajectoryFromFileMenu((TrajectoryDisplayDocument) DOCUMENT));
        trajectoryMenu.getItems().add(trajFromFileMenu);
        
        MenuItem loadReferenceTrajMenu = new MenuItem("Load reference trajectory");
        loadReferenceTrajMenu.setOnAction(new LoadReferenceTrajectoryMenu((TrajectoryDisplayDocument) DOCUMENT));
        trajectoryMenu.getItems().add(loadReferenceTrajMenu);
        MenuItem saveTrajMenu = new MenuItem("Save current trajectory");
        saveTrajMenu.setOnAction(new SaveTrajectoryMenu((TrajectoryDisplayDocument) DOCUMENT));
        trajectoryMenu.getItems().add(saveTrajMenu);

        trajectoryMenu.getItems().add(new SeparatorMenuItem());

        Menu parameterMenu = new Menu("Plot Group Layout");
        ToggleGroup parameterGroup = new ToggleGroup();
        RadioMenuItem plotHVlMenu = new RadioMenuItem("Hor.+Ver. (Line)");
        plotHVlMenu.setOnAction(new PlotParameterMenu((TrajectoryDisplayDocument) DOCUMENT, parameterGroup));
        parameterMenu.getItems().add(plotHVlMenu);
        parameterGroup.getToggles().add(plotHVlMenu);
        RadioMenuItem plotHVbMenu = new RadioMenuItem("Hor.+Ver. (Bar)");
        plotHVbMenu.setOnAction(new PlotParameterMenu((TrajectoryDisplayDocument) DOCUMENT, parameterGroup));
        parameterMenu.getItems().add(plotHVbMenu);
        parameterGroup.getToggles().add(plotHVbMenu);
        RadioMenuItem plotHCHlMenu = new RadioMenuItem("Hor. (Line) + HC");
        plotHCHlMenu.setOnAction(new PlotParameterMenu((TrajectoryDisplayDocument) DOCUMENT, parameterGroup));
        parameterMenu.getItems().add(plotHCHlMenu);
        parameterGroup.getToggles().add(plotHCHlMenu);
        RadioMenuItem plotHCHbMenu = new RadioMenuItem("Hor. (Bar) + HC");
        plotHCHbMenu.setOnAction(new PlotParameterMenu((TrajectoryDisplayDocument) DOCUMENT, parameterGroup));
        parameterMenu.getItems().add(plotHCHbMenu);
        parameterGroup.getToggles().add(plotHCHbMenu);
        RadioMenuItem plotVCVlMenu = new RadioMenuItem("Ver (Line) + VC");
        plotVCVlMenu.setOnAction(new PlotParameterMenu((TrajectoryDisplayDocument) DOCUMENT, parameterGroup));
        parameterMenu.getItems().add(plotVCVlMenu);
        parameterGroup.getToggles().add(plotVCVlMenu);
        RadioMenuItem plotVCVbMenu = new RadioMenuItem("Ver (Bar) + VC");
        plotVCVbMenu.setOnAction(new PlotParameterMenu((TrajectoryDisplayDocument) DOCUMENT, parameterGroup));
        parameterMenu.getItems().add(plotVCVbMenu);
        parameterGroup.getToggles().add(plotVCVbMenu);
        parameterGroup.selectToggle(plotHVlMenu);
        trajectoryMenu.getItems().add(parameterMenu);
        MENU_BAR.getMenus().add(MENU_BAR.getMenus().size() - 2, trajectoryMenu);
        
        java.util.prefs.Preferences defaults = Preferences.nodeForPackage(xal.ca.Channel.class);
        String DEF_PROTOCOL = "defProtocol";
        defaults.put(DEF_PROTOCOL, "CA");
        
    }

}

class TrajectoryFromFileMenu implements EventHandler {

    protected TrajectoryDisplayDocument document;
    protected ToggleGroup paramGroup;

    public TrajectoryFromFileMenu(TrajectoryDisplayDocument document) {
        this.document = document;
    }

    @Override
    public void handle(Event t) {
        document.liveTrajectory.set(false);
        document.getTrajectoryFromFile();
    }
}

class LoadReferenceTrajectoryMenu implements EventHandler {

    protected TrajectoryDisplayDocument document;

    public LoadReferenceTrajectoryMenu(TrajectoryDisplayDocument document) {
        this.document = document;
    }

    @Override
    public void handle(Event t) {
        document.loadRefTrajectory();
    }
}

class SaveTrajectoryMenu implements EventHandler {

    protected TrajectoryDisplayDocument document;

    public SaveTrajectoryMenu(TrajectoryDisplayDocument document) {
        this.document = document;
    }

    @Override
    public void handle(Event t) {
        document.saveTrajectory();
    }
}

class PlotParameterMenu implements EventHandler {

    protected TrajectoryDisplayDocument document;
    protected ToggleGroup paramGroup;

    public PlotParameterMenu(TrajectoryDisplayDocument document, ToggleGroup param) {
        this.document = document;
        this.paramGroup = param;
    }

    @Override
    public void handle(Event t) {
        RadioMenuItem menu = (RadioMenuItem) paramGroup.getSelectedToggle();
        document.parameterTrajPlot.setValue(menu.getText());
    }
}
