/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xal.app.trajectorydisplay;

import java.util.HashMap;
import java.util.List;
import xal.ca.Channel;
import xal.smf.Accelerator;
import xal.smf.AcceleratorSeq;
import xal.smf.AcceleratorSeqCombo;
import xal.smf.impl.DipoleCorr;
import xal.smf.impl.MagnetPowerSupply;

/**
 *
 * @author nataliamilas
 */
public class CorrectorArray {
    
    HashMap<DipoleCorr, Double> PosH = new HashMap();
    HashMap<DipoleCorr, Double> PosV = new HashMap();
    HashMap<DipoleCorr, Double> CV = new HashMap();
    HashMap<DipoleCorr, Double> CH = new HashMap();
    
    
    /* ------------------------
     Initialization
    * ------------------------ */
    /**
     * Initializes BPMs in the machine and try to connect
     *
     * @param accl accelerator.
     */
    public void initCorrectors(Accelerator accl) {

        List<DipoleCorr> CVList = accl.getAllNodesOfType("DCV");
        List<DipoleCorr> CHList = accl.getAllNodesOfType("DCH");
        
        CV.clear();
        PosV.clear();
        CH.clear();
        PosH.clear();        
        
        
        double max = 2;
        double min = -2;
        
        //Fill the arrays
        CVList.forEach(ele -> {
            CV.put(ele, 0.0);   
            PosV.put(ele, ele.getSDisplay());
        });
        
        CHList.forEach(ele -> {
            CH.put(ele, 0.0);   
            PosH.put(ele, ele.getSDisplay());
        });

    }
    
    /**
     * Resets the trajectory to zero
     */
    public void resetCorrectors() {

        CV.keySet().stream().forEachOrdered((ele) -> {
            CV.put(ele, 0.0);             
        });
        
        CH.keySet().stream().forEachOrdered((ele) -> {
            CH.put(ele, 0.0);             
        });
    }
    
    /**
     * Sets value of the vertical corrector current     
     *
     * @param cvEle vertical corrector.
     * @param val current value.
     */
    public void setCV(DipoleCorr cvEle, double val) {
        CV.put(cvEle, val);
    }
    
    /**
     * Sets value of the vertical corrector current     
     *
     * @param chEle vertical corrector.
     * @param val current value.
     */
    public void setCH(DipoleCorr chEle, double val) {
        CH.put(chEle, val);
    }
    
    /**
     * Gets value of the vertical corrector current     
     *
     * @param cvEle vertical corrector.
     * @return value of the corrector current
     */
    public double getCV(DipoleCorr cvEle) {
        return CV.get(cvEle);
    }
    
    /**
     * Gets value of the vertical corrector current     
     *
     * @param chEle vertical corrector.
     * @return value of the corrector current
     */
    public double getCH(DipoleCorr chEle) {
        return CH.get(chEle);
    }

    
    /**
     * Lists channels related to horizontal trajectory
     *
     * @param Sequence accelerator sequence.
     * @return hashMap of input channels
     */
    public HashMap<Channel,DipoleCorr> getCVChannels(AcceleratorSeq Sequence) {

        List<DipoleCorr> CVList = Sequence.getAllNodesOfType("DCV");

        HashMap<Channel,DipoleCorr> inputChannels = new HashMap();

        CVList.forEach((DipoleCorr ele) -> {            
            inputChannels.put(ele.getMainSupply().getChannel(MagnetPowerSupply.CURRENT_RB_HANDLE),ele);
            //inputChannels.put(ele.getChannel(DipoleCorr.FIELD_RB_HANDLE),ele);
        });

        return inputChannels;
    }
    
    /**
     * Lists channels related to horizontal trajectory
     *
     * @param ComboSequence accelerator sequence.
     * @return hashMap of input channels
     */
    public HashMap<Channel,DipoleCorr> getCVChannels(AcceleratorSeqCombo ComboSequence) {

        List<DipoleCorr> CVList = ComboSequence.getAllNodesOfType("DCV");

        HashMap<Channel,DipoleCorr> inputChannels = new HashMap();

        CVList.forEach((DipoleCorr ele) -> {            
            inputChannels.put(ele.getMainSupply().getChannel(MagnetPowerSupply.CURRENT_RB_HANDLE),ele);
            //inputChannels.put(ele.getChannel(DipoleCorr.FIELD_RB_HANDLE),ele);
        });

        return inputChannels;
    }
    
    /**
     * Lists channels related to horizontal correctors
     *
     * @param Sequence accelerator sequence.
     * @return hashMap of input channels
     */
    public HashMap<Channel,DipoleCorr> getCHChannels(AcceleratorSeq Sequence) {

        List<DipoleCorr> CHList = Sequence.getAllNodesOfType("DCH");

        HashMap<Channel,DipoleCorr> inputChannels = new HashMap();

        CHList.forEach((DipoleCorr ele) -> {            
            inputChannels.put(ele.getMainSupply().getChannel(MagnetPowerSupply.CURRENT_RB_HANDLE),ele);
            //inputChannels.put(ele.getChannel(DipoleCorr.FIELD_RB_HANDLE),ele);
        });

        return inputChannels;
    }
    
    /**
     * Lists channels related to horizontal correctors
     *
     * @param ComboSequence accelerator sequence.
     * @return hashMap of input channels
     */
    public HashMap<Channel,DipoleCorr> getCHChannels(AcceleratorSeqCombo ComboSequence) {

        List<DipoleCorr> CHList = ComboSequence.getAllNodesOfType("DCH");

        HashMap<Channel,DipoleCorr> inputChannels = new HashMap();

        CHList.forEach((DipoleCorr ele) -> {            
            inputChannels.put(ele.getMainSupply().getChannel(MagnetPowerSupply.CURRENT_RB_HANDLE),ele);
            //inputChannels.put(ele.getChannel(DipoleCorr.FIELD_RB_HANDLE),ele);
        });

        return inputChannels;
    }
    
    /**
     * List all Vertical Correctors
     *
     * @param Sequence accelerator sequence.
     * @return list with all CVs in the sequence
     */
    public List<DipoleCorr> getCVs(AcceleratorSeq Sequence) {

         return Sequence.getAllNodesOfType("DCV");
    }
    
    /**
     * List all Horizontal Correctors
     *
     * @param Sequence accelerator sequence.
     * @return list with all CVs in the sequence
     */
    public List<DipoleCorr> getCHs(AcceleratorSeq Sequence) {

         return Sequence.getAllNodesOfType("DCH");
    }
    
    /**
     * List all Vertical Correctors
     *
     * @param ComboSequence accelerator sequence.
     * @return list with all CVs in the sequence
     */
    public List<DipoleCorr> getCVs(AcceleratorSeqCombo ComboSequence) {

         return ComboSequence.getAllNodesOfType("DCV");
    }
    
    /**
     * List all Horizontal Correctors
     *
     * @param ComboSequence accelerator sequence.
     * @return list with all CVs in the sequence
     */
    public List<DipoleCorr> getCHs(AcceleratorSeqCombo ComboSequence) {

         return ComboSequence.getAllNodesOfType("DCH");
    }
    
    /**
     * Return the minimum distance between horizontal correctors
     *
     * @param ComboSequence accelerator sequence.
     * @return list with all CVs in the sequence
     */
    public double getminDistanceCHs(AcceleratorSeqCombo ComboSequence) {
        
        List<DipoleCorr> CHList = ComboSequence.getAllNodesOfType("DCH");               
        
        double ele1=CHList.get(0).getSDisplay();
        double ele2;
        double minDist = ele1;
        
        for (int i = 1; i < CHList.size(); i++) {      
            ele2 = CHList.get(i).getSDisplay();
            if (ele2-ele1 < minDist ){
                minDist = ele2-ele1;
            }
            ele1 = ele2;
        }

        return minDist;
    }
    
    /**
     * Return the minimum distance between horizontal correctors
     *
     * @param Sequence accelerator sequence.
     * @return list with all CVs in the sequence
     */
    public double getminDistanceCHs(AcceleratorSeq Sequence) {
        
        List<DipoleCorr> CHList = Sequence.getAllNodesOfType("DCH");               
        
        double ele1=CHList.get(0).getSDisplay();
        double ele2;
        double minDist = ele1;
        
        for (int i = 1; i < CHList.size(); i++) {      
            ele2 = CHList.get(i).getSDisplay();
            if (ele2-ele1 < minDist ){
                minDist = ele2-ele1;
            }
            ele1 = ele2;
        }

        return minDist;
    }
    
    /**
     * Return the minimum distance between horizontal correctors
     *
     * @param CHList of hor correctors
     * @return list with all CVs in the sequence
     */
    public double getminDistanceCHs(List<DipoleCorr> CHList) {
               
        double ele1=CHList.get(0).getSDisplay();
        double ele2;
        double minDist = ele1;
        
        for (int i = 1; i < CHList.size(); i++) {      
            ele2 = CHList.get(i).getSDisplay();
            if (ele2-ele1 < minDist ){
                minDist = ele2-ele1;
            }
            ele1 = ele2;
        }

        return minDist;
    }
    
    
    /**
     * Return the minimum distance between vertical correctors
     *
     * @param ComboSequence accelerator sequence.
     * @return list with all CVs in the sequence
     */
    public double getminDistanceCVs(AcceleratorSeqCombo ComboSequence) {
        
        List<DipoleCorr> CVList = ComboSequence.getAllNodesOfType("DCV");               
        
        double ele1=CVList.get(0).getSDisplay();
        double ele2;
        double minDist = ele1;
        
        for (int i = 1; i < CVList.size(); i++) {      
            ele2 = CVList.get(i).getSDisplay();
            if (ele2-ele1 < minDist ){
                minDist = ele2-ele1;
            }
            ele1 = ele2;
        }

        return minDist;
    }
    
    /**
     * Return the minimum distance between vertical correctors
     *
     * @param Sequence accelerator sequence.
     * @return list with all CVs in the sequence
     */
    public double getminDistanceCVs(AcceleratorSeq Sequence) {
        
        List<DipoleCorr> CVList = Sequence.getAllNodesOfType("DCV");               
        
        double ele1=CVList.get(0).getSDisplay();
        double ele2;
        double minDist = ele1;
        
        for (int i = 1; i < CVList.size(); i++) {      
            ele2 = CVList.get(i).getSDisplay();
            if (ele2-ele1 < minDist ){
                minDist = ele2-ele1;
            }
            ele1 = ele2;
        }

        return minDist;
    }
    
    /**
     * Return the minimum distance between vertical correctors
     *
     * @param CVList list of correctors.
     * @return list with all CVs in the sequence
     */
    public double getminDistanceCVs(List<DipoleCorr> CVList) {
                
        double ele1=CVList.get(0).getSDisplay();
        double ele2;
        double minDist = ele1;
        
        for (int i = 1; i < CVList.size(); i++) {      
            ele2 = CVList.get(i).getSDisplay();
            if (ele2-ele1 < minDist ){
                minDist = ele2-ele1;
            }
            ele1 = ele2;
        }

        return minDist;
    }
    
}
