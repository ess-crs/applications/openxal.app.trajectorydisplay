/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.trajectorydisplay;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.EventHandler;
import xal.ca.Monitor;

/**
 * Coalescing handler for EPICS monitors.
 *
 * Subclasses must implement the initializeMonitors method, where EPICS monitors are started.
 *
 * When the subclass wants to trigger the event handler, it must call notifyNewUpdate().
 *
 * If notifyNewUpdate() is called several times in a short time (<100 ms + handle() computing time), the handle method
 * will only be called once.
 *
 * @author Juan F. Esteban Müller
 * <JuanF.EstebanMuller@ess.eu>
 */
public abstract class AbstractMonitorsHandler {

    protected final TrajectoryArray trajectory;
    protected final CorrectorArray correctors; 
    protected final EventHandler handler;
    protected List<xal.ca.Monitor> monitors = new ArrayList<>();
    protected final Object lock = new Object();
    protected Thread loop;
    protected boolean stopFlag = true;
    protected boolean newUpdates = false;
    protected int syncTime = 1000;

    public AbstractMonitorsHandler(TrajectoryArray trajectory,CorrectorArray correctors, EventHandler handler) {
        this.trajectory = trajectory;
        this.correctors = correctors;
        this.handler = handler;
    }

    public void stop() {
        if(!monitors.isEmpty()){
            for (xal.ca.Monitor monitor : monitors) {
                try {
                    monitor.clear();
                } catch (NullPointerException ex) {
                    Logger.getLogger(LiveChannelMonitor.class.getName()).log(Level.FINE, "NullPointerException while stopping monitor to channel " + monitor.getChannel().channelName() + ".", ex);
                } catch (Exception ex) {
                    Logger.getLogger(AbstractMonitorsHandler.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            try {
                stopFlag = true;
                synchronized (lock) {
                    lock.notifyAll();
                }
                loop.join();
            } catch (InterruptedException ex) {
                Logger.getLogger(AbstractMonitorsHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void start() {
        loop = new Thread(this::loop);
        stopFlag = false;
        loop.start();
    }
    
    public void setSyncTime(int val) {
        syncTime = val;
    }

    protected abstract void initializeMonitors();

    private void loop() {
        initializeMonitors();

        while (!stopFlag) {
            try {
                synchronized (lock) {
                    if (!newUpdates) {
                        lock.wait();
                        if (stopFlag) {
                            return;
                        }
                    }
                }

                // Delaying 100 ms to wait for other events, to avoid calling continuously the handle method.
                try {
                    Thread.sleep(syncTime);
                } catch (InterruptedException ex) {
                    Logger.getLogger(LiveChannelMonitor.class.getName()).log(Level.SEVERE, null, ex);
                }

                synchronized (lock) {
                    newUpdates = false;
                }

                // New updates can arrive while executing handle method
                handle();
            } catch (InterruptedException ex) {
                Logger.getLogger(AbstractMonitorsHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    protected void handle() {
        handler.handle(null);
    }

    protected void notifyNewUpdate() {
        synchronized (lock) {
            newUpdates = true;
            lock.notifyAll();
        }
    }
}