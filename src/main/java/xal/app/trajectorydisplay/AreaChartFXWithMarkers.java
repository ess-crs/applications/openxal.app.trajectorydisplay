package xal.app.trajectorydisplay;

import eu.ess.xaos.ui.plot.AreaChartFX;
import java.util.Objects;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.Axis;
import javafx.scene.chart.XYChart;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author nataliamilas
 */
public class AreaChartFXWithMarkers <X,Y> extends AreaChartFX {

    private final ObservableList<XYChart.Data<X, Y>> horizontalMarkers;
    private final ObservableList<XYChart.Data<X, Y>> verticalMarkers;
    private final ObservableList<XYChart.Data<X, X>> horizontalRangeMarkers;
    private final ObservableList<XYChart.Data<X, X>> verticalRangeMarkers;


    public AreaChartFXWithMarkers(Axis<X> xAxis, Axis<Y> yAxis) {
        super(xAxis, yAxis);
        horizontalMarkers = FXCollections.observableArrayList(data -> new Observable[] {data.YValueProperty()});
        horizontalMarkers.addListener((InvalidationListener)observable -> layoutPlotChildren());
        
        verticalMarkers = FXCollections.observableArrayList(data -> new Observable[] {data.XValueProperty()});
        verticalMarkers.addListener((InvalidationListener)observable -> layoutPlotChildren());
        
        verticalRangeMarkers = FXCollections.observableArrayList(data -> new Observable[] {data.XValueProperty()});
        verticalRangeMarkers.addListener((InvalidationListener)observable -> layoutPlotChildren());
        
        horizontalRangeMarkers = FXCollections.observableArrayList(data -> new Observable[] {data.YValueProperty()});
        horizontalRangeMarkers.addListener((InvalidationListener)observable -> layoutPlotChildren());
    }

    public void addHorizontalValueMarker(XYChart.Data<X, Y> marker) {
        Objects.requireNonNull(marker, "the marker must not be null");
        if (horizontalMarkers.contains(marker)) return;
        Line line = new Line();
        marker.setNode(line );
        getPlotChildren().add(line);
        horizontalMarkers.add(marker);
    }

    public void removeHorizontalValueMarker(XYChart.Data<X, Y> marker) {
        Objects.requireNonNull(marker, "the marker must not be null");
        if (marker.getNode() != null) {
            getPlotChildren().remove(marker.getNode());
            marker.setNode(null);
        }
        horizontalMarkers.remove(marker);
    }

    public void addVerticalValueMarker(XYChart.Data<X, Y> marker) {
        Objects.requireNonNull(marker, "the marker must not be null");
        if (verticalMarkers.contains(marker)) return;
        Line line = new Line();
        marker.setNode(line );
        getPlotChildren().add(line);
        verticalMarkers.add(marker);
    }

    public void removeVerticalValueMarker(XYChart.Data<X, Y> marker) {
        Objects.requireNonNull(marker, "the marker must not be null");
        if (marker.getNode() != null) {
            getPlotChildren().remove(marker.getNode());
            marker.setNode(null);
        }
        verticalMarkers.remove(marker);
    }
    
    public void addVerticalRangeMarker(XYChart.Data<X, X> marker) {
    Objects.requireNonNull(marker, "the marker must not be null");
    if (verticalRangeMarkers.contains(marker)) return;

    Rectangle rectangle = new Rectangle(0,0,0,0);
    rectangle.setStroke(Color.TRANSPARENT);
    rectangle.setFill(Color.GREY.deriveColor(1, 1, 1, 0.2));

    marker.setNode( rectangle);

    getPlotChildren().add(rectangle);
    verticalRangeMarkers.add(marker);
    }

    public void removeVerticalRangeMarker(XYChart.Data<X, X> marker) {
        Objects.requireNonNull(marker, "the marker must not be null");
        if (marker.getNode() != null) {
            getPlotChildren().remove(marker.getNode());
            marker.setNode(null);
        }
        verticalRangeMarkers.remove(marker);
    }

    public void addHorizontalRangeMarker(XYChart.Data<X, X> marker) {
        Objects.requireNonNull(marker, "the marker must not be null");
        if (horizontalRangeMarkers.contains(marker)) return;

        Rectangle rectangle = new Rectangle(0,0,0,0);
        rectangle.setStroke(Color.TRANSPARENT);
        rectangle.setFill(Color.GREY.deriveColor(1, 1, 1, 0.2));

        marker.setNode( rectangle);

        getPlotChildren().add(rectangle);
        horizontalRangeMarkers.add(marker);
    }

    public void removeHorizontalRangeMarker(XYChart.Data<X, X> marker) {
        Objects.requireNonNull(marker, "the marker must not be null");
        if (marker.getNode() != null) {
            getPlotChildren().remove(marker.getNode());
            marker.setNode(null);
        }
        horizontalRangeMarkers.remove(marker);
    }

    public void removeAllMarkers(){
        for (XYChart.Data<X, Y> markerH : horizontalMarkers) {
            getPlotChildren().remove(markerH.getNode());
            markerH.setNode(null);        
        }
        horizontalMarkers.clear();
        
        for (XYChart.Data<X, Y> markerV : verticalMarkers) {
            getPlotChildren().remove(markerV.getNode());
            markerV.setNode(null);        
        }
        verticalMarkers.clear();
        for (XYChart.Data<X, X> markerRH : horizontalRangeMarkers) {
            getPlotChildren().remove(markerRH.getNode());
            markerRH.setNode(null);        
        }
        horizontalRangeMarkers.clear();
        
        for (XYChart.Data<X, X> markerRV : verticalRangeMarkers) {
            getPlotChildren().remove(markerRV.getNode());
            markerRV.setNode(null);        
        }
        verticalRangeMarkers.clear();
        
    }

    @Override
    protected void layoutPlotChildren() {
        super.layoutPlotChildren();
        for (XYChart.Data<X, Y> horizontalMarker : horizontalMarkers) {
            Line line = (Line) horizontalMarker.getNode();
            line.setStartX(0);
            line.setEndX(getBoundsInLocal().getWidth());
            line.setStartY(getYAxis().getDisplayPosition(horizontalMarker.getYValue()) + 0.5); // 0.5 for crispness
            line.setEndY(line.getStartY());
            line.toFront();
        }
        for (XYChart.Data<X, Y> verticalMarker : verticalMarkers) {
            Line line = (Line) verticalMarker.getNode();
            line.setStartX(getXAxis().getDisplayPosition(verticalMarker.getXValue()) + 0.5);  // 0.5 for crispness
            line.setEndX(line.getStartX());
            line.setStartY(0d);
            line.setEndY(getBoundsInLocal().getHeight());
            line.toFront();
        }      
        for (XYChart.Data<X, X> verticalRangeMarker : verticalRangeMarkers) {
            Rectangle rectangle = (Rectangle) verticalRangeMarker.getNode();
            rectangle.setX( getXAxis().getDisplayPosition(verticalRangeMarker.getXValue()) + 0.5);  // 0.5 for crispness
            rectangle.setWidth( getXAxis().getDisplayPosition(verticalRangeMarker.getYValue()) - getXAxis().getDisplayPosition(verticalRangeMarker.getXValue()));
            rectangle.setY(0d);
            rectangle.setHeight(getBoundsInLocal().getHeight());
            rectangle.toBack();
        }
        for (XYChart.Data<X, X> horizontalRangeMarker : horizontalRangeMarkers) {
            Rectangle rectangle = (Rectangle) horizontalRangeMarker.getNode();
            rectangle.setX( getXAxis().getDisplayPosition(horizontalRangeMarker.getXValue()) + 0.5);  // 0.5 for crispness
            rectangle.setWidth( getXAxis().getDisplayPosition(horizontalRangeMarker.getYValue()) - getXAxis().getDisplayPosition(horizontalRangeMarker.getXValue()));
            rectangle.setY(0d);
            rectangle.setHeight(getBoundsInLocal().getHeight());
            rectangle.toBack();
        }
    }

}

