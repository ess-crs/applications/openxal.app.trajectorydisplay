/*
 * Copyright (C) 2018 European Spallation Source ERIC
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.trajectorydisplay;

/**
 *
 * @author nataliamilas
 */

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;

public class CircularArrayList<Number> extends AbstractList<Number> implements RandomAccess {

private int n; // buffer length
private List<Number> buf; // a List implementing RandomAccess
private int leader = 0;
private int size = 0;


public CircularArrayList(int capacity) {
    n = capacity + 1;
    buf = new ArrayList<Number>(Collections.nCopies(n, (Number) null));
}

public int capacity() {
    return n - 1;
}

private int wrapIndex(int i) {
    int m = i % n;
    if (m < 0) { // modulus can be negative
        m += n;
    }
    return m;
}

@Override
public int size() {
    return this.size;
}

@Override
public Number get(int i) {
    if (i < 0 || i >= n-1) throw new IndexOutOfBoundsException();

    if(i > size()) throw new NullPointerException("Index is greater than size.");

    return buf.get(wrapIndex(leader + i));
}

@Override
public Number set(int i, Number e) {
    if (i < 0 || i >= n-1) {
        throw new IndexOutOfBoundsException();
    }
    if(i == size()) // assume leader's position as invalid (should use insert(e))
        throw new IndexOutOfBoundsException("The size of the list is " + size() + " while the index was " + i
                +". Please use insert(e) method to fill the list.");
    return buf.set(wrapIndex(leader - size + i), e);
}

public void insert(Number e)
{
    int s = size();
    buf.set(wrapIndex(leader), e);
    leader = wrapIndex(++leader);
    buf.set(leader, null);
    if(s == n-1)
        return; // we have replaced the eldest element.
    this.size++;

}

@Override
public void clear()
{
    int cnt = wrapIndex(leader-size());
    for(; cnt != leader; cnt = wrapIndex(++cnt))
        this.buf.set(cnt, null);
    this.size = 0;
}

public Number removeOldest() {
    int i = wrapIndex(leader+1);

    for(;;i = wrapIndex(++i)) {
        if(buf.get(i) != null) break;
        if(i == leader)
            throw new IllegalStateException("Cannot remove element."
                    + " CircularArrayList is empty.");
    }

    this.size--;
    return buf.set(i, null);
}

@Override
public String toString()
{
    int i = wrapIndex(leader - size());
    StringBuilder str = new StringBuilder(size());

    for(; i != leader; i = wrapIndex(++i)){
        str.append(buf.get(i));
    }
    return str.toString();
}

public Number getOldest(){
    int i = wrapIndex(leader+1);

    for(;;i = wrapIndex(++i)) {
        if(buf.get(i) != null) break;
        if(i == leader)
            throw new IllegalStateException("Cannot remove element."
                    + " CircularArrayList is empty.");
    }

    return buf.get(i);
}

public Number getNewest(){
    int i = wrapIndex(leader-1);
    if(buf.get(i) == null)
        throw new IndexOutOfBoundsException("Error while retrieving the newest element. The Circular Array list is empty.");
    return buf.get(i);
}

public void resize(int capacity){
    clear();
    n = capacity + 1;
    buf = new ArrayList<Number>(Collections.nCopies(n, (Number) null));
}

public double getAverage(){
    double sum = 0;
    int length= 0;

    for(int k=0; k<size; k++){
        if(buf.get(k) != null){
            sum = (double) buf.get(k) + sum;
            length +=1;
        }
    }

    if (length == 0) {length = 1;};

    return sum/length;

}

public double getRms(){
    double sum = 0;
    double mean = this.getAverage();
    int length= 0;

    for(int k=0; k<size; k++){
        if(buf.get(k) != null){
            sum = ((double) buf.get(k) - mean)*((double) buf.get(k) - mean);
            length +=1;
        }
    }

    if (length == 0) {length = 1;};

    return Math.sqrt(sum/length);

}

public double getRelativeMax(){
    double max = 0;

    for(int k=0; k<size; k++){
        if((double)buf.get(k) > max){
            max = (double)buf.get(k);
        }
    }

    return Math.abs(max - getAverage());

}

public double getRelativeMin(){
    double min = 0;

    for(int k=0; k<size; k++){
        if((double)buf.get(k) < min){
            min = (double)buf.get(k);
        }
    }

    return Math.abs(getAverage() - min);

}


}
