/*
 * Copyright (C) 2018 European Spallation Source ERIC
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.trajectorydisplay;

import eu.ess.xaos.ui.plot.LineChartFX;
import eu.ess.xaos.ui.plot.PluggableChartContainer;
import eu.ess.xaos.ui.plot.plugins.Pluggable;
import eu.ess.xaos.ui.plot.plugins.Plugins;
import eu.ess.xaos.ui.plot.util.SeriesColorUtils;
import eu.ess.xaos.ui.util.ColorUtils;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.chart.ValueAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import xal.ca.Channel;
import xal.ca.ConnectionException;
import xal.ca.GetException;
import xal.extension.fxapplication.Controller;
import xal.smf.Accelerator;
import xal.smf.AcceleratorSeq;
import xal.smf.impl.BPM;
import xal.smf.impl.DipoleCorr;

public class FXMLController extends Controller {

    //Trajectory to be displayed on the plot
    public TrajectoryArray DisplayTraj = new TrajectoryArray();
    public CorrectorArray DisplayCorr = new CorrectorArray();
    private final TrajectoryArray ReferenceTraj = new TrajectoryArray();

    //Setup the Plot
    private final XYChart.Series<Number, Number> trajXSeries = new XYChart.Series<>("x", FXCollections.observableArrayList());
    private final XYChart.Series<Number, Number> trajYSeries = new XYChart.Series<>("y", FXCollections.observableArrayList());
    private final XYChart.Series<Number, Number> trajXSeriesBar = new XYChart.Series<>("x", FXCollections.observableArrayList());
    private final XYChart.Series<Number, Number> trajYSeriesBar = new XYChart.Series<>("y", FXCollections.observableArrayList());
    private final XYChart.Series<Number, Number> refXSeries = new XYChart.Series<>("reference x", FXCollections.observableArrayList());
    private final XYChart.Series<Number, Number> refYSeries = new XYChart.Series<>("reference y", FXCollections.observableArrayList());
    
    //Corrector Bar charts
    private final XYChart.Series<Number, Number> CVSeries = new XYChart.Series<>("CH", FXCollections.observableArrayList());
    private final XYChart.Series<Number, Number> CHSeries = new XYChart.Series<>("CV", FXCollections.observableArrayList());
  
    //BPm amplitude
    private final XYChart.Series<Number, Number> AmplSeries = new XYChart.Series<>("amplitude", FXCollections.observableArrayList());  
    private final XYChart.Series<Number, Number> refAmplSeries = new XYChart.Series<>("ref. amplitude", FXCollections.observableArrayList());   
          
    //private final HashMap<BPM,CircularArrayList<Number>> bufferXData = new HashMap();
    //private final HashMap<BPM,CircularArrayList<Number>> bufferYData = new HashMap();
    //private final HashMap<BPM,CircularArrayList<Number>> bufferAmplData = new HashMap();

    private final PluggableChartContainer chartContainer1 = new PluggableChartContainer();
    private final PluggableChartContainer chartContainer2 = new PluggableChartContainer();
    private final PluggableChartContainer chartContainer1b = new PluggableChartContainer();
    private final PluggableChartContainer chartContainer2b = new PluggableChartContainer();
    private final PluggableChartContainer chartContainer3 = new PluggableChartContainer();
    private final PluggableChartContainer chartContainer4 = new PluggableChartContainer();
    private final PluggableChartContainer chartContainer5 = new PluggableChartContainer();

    private LineChartFXWithMarkers<Number, Number> lineChart1;
    private LineChartFXWithMarkers<Number, Number> lineChart2;
    private LineChartFXWithMarkers<Number, Number> lineChart3;
    private AreaChartFXWithMarkers<Number, Number> barChart1;
    private AreaChartFXWithMarkers<Number, Number> barChart2;
    private AreaChartFXWithMarkers<Number, Number> barChart4;
    private AreaChartFXWithMarkers<Number, Number> barChart5;

    //private final ChannelMonitor monitor = new ChannelMonitor();
    
    private final Color colorX = ColorUtils.ESS_RED;
    private final Color colorXref = Color.rgb(249, 139, 133);
    private final Color colorY = SeriesColorUtils.VERTICAL;
    private final Color colorYref = ColorUtils.ESS_PRIMARY_LIGHT;        
    private final Color colorAmpl = ColorUtils.ESS_ORANGE;
    private final Color colorAmplref = ColorUtils.ESS_YELLOW;   
    private final Color colorCV = Color.rgb(249, 139, 133);
    private final Color colorCH = ColorUtils.ESS_PRIMARY_LIGHT;   
    
    private LiveChannelMonitor monitor;    

    @FXML
    private Label labelXrms;
    @FXML
    private Label labelYrms;
    @FXML
    private StackPane chartPane1;
    @FXML
    private StackPane chartPane2;
    @FXML
    private StackPane chartPane3;
    @FXML
    private ComboBox<String> comboBoxRefTrajectory;
    @FXML
    private ChoiceBox<Double> comboPersistence;
    @FXML
    private Button buttonPauseTrajectoryLive;
    @FXML
    private CheckBox noLine;
    @FXML
    private Label labelTrajectoryStatus;

    public LineChartFX<Number, Number> getLineChart1() {
        return lineChart1;
    }

    public LineChartFX<Number, Number> getLineChart2() {
        return lineChart2;
    }
    
    public LineChartFX<Number, Number> getLineChart3() {
        return lineChart3;
    }

    public XYChart.Series<Number, Number> getXSeries() {
        return trajXSeries;
    }

    public XYChart.Series<Number, Number> getYSeries() {
        return trajYSeries;
    }

    public XYChart.Series<Number, Number> getAmplSeries() {
        return AmplSeries;
    }
    
    public XYChart.Series<Number, Number> getCVSeries() {
        return CVSeries;
    }
    
    public XYChart.Series<Number, Number> getCHSeries() {
        return CHSeries;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        //Initialize classes to stor traj and correctors
        DisplayTraj.initBPMs(MainFunctions.mainDocument.getAccelerator());
        DisplayCorr.initCorrectors(MainFunctions.mainDocument.getAccelerator());
        ReferenceTraj.initBPMs(MainFunctions.mainDocument.getAccelerator());        
        
        //buttonPauseTrajectoryLive.setVisible(false);
        //buttonPauseTrajectoryLive.setText("PAUSE");
        
        EventHandler<ActionEvent> updatePlot = t -> { 
            Platform.runLater(()->{
            String getSeqName = MainFunctions.mainDocument.getSequence();
            if (MainFunctions.mainDocument.getAccelerator().getSequence(getSeqName) != null && MainFunctions.mainDocument.liveTrajectory.get()) {
                updateDataset(DisplayTraj.getBPMs(MainFunctions.mainDocument.getAccelerator().getSequence(getSeqName)),DisplayCorr.getCVs(MainFunctions.mainDocument.getAccelerator().getSequence(getSeqName)),
                    DisplayCorr.getCHs(MainFunctions.mainDocument.getAccelerator().getSequence(getSeqName)));
            } else if (MainFunctions.mainDocument.getAccelerator().getComboSequence(getSeqName) != null && MainFunctions.mainDocument.liveTrajectory.get()) {
                updateDataset(DisplayTraj.getBPMs(MainFunctions.mainDocument.getAccelerator().getComboSequence(getSeqName)),DisplayCorr.getCVs(MainFunctions.mainDocument.getAccelerator().getComboSequence(getSeqName)),
                    DisplayCorr.getCHs(MainFunctions.mainDocument.getAccelerator().getComboSequence(getSeqName)));
            }});            
            
        };
        
        //create an instance of the monitor for the channels
        monitor = new LiveChannelMonitor(DisplayTraj, DisplayCorr,updatePlot);
        
        HashMap<Channel,BPM> inputChannelsX;
        HashMap<Channel,BPM> inputChannelsY;
        HashMap<Channel,BPM> inputChannelsAmpl;
        HashMap<Channel,DipoleCorr> inputChannelsCV;
        HashMap<Channel,DipoleCorr> inputChannelsCH;
       
        inputChannelsX = DisplayTraj.getBPMChannelsX(MainFunctions.mainDocument.getAccelerator());
        inputChannelsY = DisplayTraj.getBPMChannelsY(MainFunctions.mainDocument.getAccelerator());
        inputChannelsAmpl = DisplayTraj.getBPMChannelsAmpl(MainFunctions.mainDocument.getAccelerator());
        inputChannelsCV = DisplayCorr.getCVChannels(MainFunctions.mainDocument.getAccelerator());
        inputChannelsCH = DisplayCorr.getCHChannels(MainFunctions.mainDocument.getAccelerator());

        monitor.updatechannels(inputChannelsX, inputChannelsY, inputChannelsAmpl, inputChannelsCV, inputChannelsCH);          
                
        //populate the ComboBox element
        comboBoxRefTrajectory.getItems().addAll("Zero Trajectory");
        comboBoxRefTrajectory.setValue("Zero Trajectory");        

        //read new reference trajectory file
        try {
            DisplayTraj.loadReferenceTrajectory(comboBoxRefTrajectory.getSelectionModel().getSelectedItem());
            ReferenceTraj.loadReferenceTrajectory(comboBoxRefTrajectory.getSelectionModel().getSelectedItem());
        } catch (IOException ex) {
            Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }

        //Create rms lables
        String nameHor = "x_rms = 0.0 um";
        String nameVer = "y_rms = 0.0 um";

        //Initialize the combo Box for persistence
        comboPersistence.getItems().add(0, 1.0);
        comboPersistence.getItems().add(1, 2.0);
        comboPersistence.getItems().add(2, 3.5);
        comboPersistence.getItems().add(3, 7.0);
        comboPersistence.getItems().add(4, 14.0);
        comboPersistence.setValue(1.0);

        ValueAxis<Number> xAxis1 = new eu.ess.xaos.ui.plot.NumberAxis();
        ValueAxis<Number> xAxis2 = new eu.ess.xaos.ui.plot.NumberAxis();
        ValueAxis<Number> xAxis1b = new eu.ess.xaos.ui.plot.NumberAxis();
        ValueAxis<Number> xAxis2b = new eu.ess.xaos.ui.plot.NumberAxis();
        ValueAxis<Number> xAxis3 = new eu.ess.xaos.ui.plot.NumberAxis();
        ValueAxis<Number> xAxis4 = new eu.ess.xaos.ui.plot.NumberAxis();
        ValueAxis<Number> xAxis5 = new eu.ess.xaos.ui.plot.NumberAxis();
        ValueAxis<Number> yAxis1 = new eu.ess.xaos.ui.plot.NumberAxis();
        ValueAxis<Number> yAxis2 = new eu.ess.xaos.ui.plot.NumberAxis();
        ValueAxis<Number> yAxis1b = new eu.ess.xaos.ui.plot.NumberAxis();
        ValueAxis<Number> yAxis2b = new eu.ess.xaos.ui.plot.NumberAxis();
        ValueAxis<Number> yAxis3 = new eu.ess.xaos.ui.plot.NumberAxis();
        ValueAxis<Number> yAxis4 = new eu.ess.xaos.ui.plot.NumberAxis();
        ValueAxis<Number> yAxis5 = new eu.ess.xaos.ui.plot.NumberAxis();        

        xAxis1.setLabel("Position [m]");
        yAxis1.setLabel("Horizontal Trajectory [um]");
        xAxis1b.setLabel("Position [m]");
        yAxis1b.setLabel("Horizontal Trajectory [um]");
        xAxis2.setLabel("Position [m]");
        yAxis2.setLabel("Vertical Trajectory [um]");
        xAxis2b.setLabel("Position [m]");
        yAxis2b.setLabel("Vertical Trajectory [um]");
        xAxis3.setLabel("Position [m]");
        yAxis3.setLabel("BPM Amplitude [a.u.]");
        xAxis4.setLabel("Position [m]");
        yAxis4.setLabel("Horizontal Corr. Current [A]");
        xAxis5.setLabel("Position [m]");
        yAxis5.setLabel("Vertical Corr. Current [A]");


        xAxis1.setAnimated(false);
        xAxis2.setAnimated(false);
        xAxis1b.setAnimated(false);
        xAxis2b.setAnimated(false);
        xAxis3.setAnimated(false);
        xAxis4.setAnimated(false);
        xAxis5.setAnimated(false);
        yAxis1.setAnimated(false);
        yAxis2.setAnimated(false);
        yAxis1b.setAnimated(false);
        yAxis2b.setAnimated(false);
        yAxis3.setAnimated(false);
        yAxis4.setAnimated(false);
        yAxis5.setAnimated(false);
        
        //Initializes the chart and chartData
        labelXrms.setText(nameHor);
        labelYrms.setText(nameVer);

        trajXSeries.setName("x");
        trajYSeries.setName("y");        
        trajXSeriesBar.setName("x");
        trajYSeriesBar.setName("y");        
        refXSeries.setName("ref. x");
        refYSeries.setName("ref. y");        
        CHSeries.setName("Hor. Corr.");               
        CVSeries.setName("Vert. Corr.");      
        AmplSeries.setName("BPM Amplitude");
        refAmplSeries.setName("ref. BPM Amplitude");
                
        lineChart1 = new LineChartFXWithMarkers<>(xAxis1, yAxis1);

        lineChart1.setAnimated(false);
        lineChart1.setOnMouseClicked(event -> lineChart1.requestFocus());
        lineChart1.addChartPlugins(FXCollections.observableList(List.of(Plugins.all())));
        
        lineChart2 = new LineChartFXWithMarkers<>(xAxis2, yAxis2);

        lineChart2.setAnimated(false);
        lineChart2.setOnMouseClicked(event -> lineChart2.requestFocus());
        lineChart2.addChartPlugins(FXCollections.observableList(List.of(Plugins.all())));
                                    
        lineChart3 = new LineChartFXWithMarkers<>(xAxis3, yAxis3);

        lineChart3.setAnimated(false);
        lineChart3.setOnMouseClicked(event -> lineChart3.requestFocus());
        lineChart3.addChartPlugins(FXCollections.observableList(List.of(Plugins.all())));
        
        barChart1 = new AreaChartFXWithMarkers<>(xAxis1b, yAxis1b);

        barChart1.setAnimated(false);
        barChart1.setOnMouseClicked(event -> barChart1.requestFocus());
        barChart1.addChartPlugins(FXCollections.observableList(List.of(Plugins.all())));
        
        barChart2 = new AreaChartFXWithMarkers<>(xAxis2b, yAxis2b);

        barChart2.setAnimated(false);
        barChart2.setOnMouseClicked(event -> barChart2.requestFocus());
        barChart2.addChartPlugins(FXCollections.observableList(List.of(Plugins.all())));
        
        barChart4 = new AreaChartFXWithMarkers<>(xAxis4, yAxis4);

        barChart4.setAnimated(false);
        barChart4.setOnMouseClicked(event -> barChart4.requestFocus());
        barChart4.addChartPlugins(FXCollections.observableList(List.of(Plugins.all())));
        
        barChart5 = new AreaChartFXWithMarkers<>(xAxis5, yAxis5);

        barChart5.setAnimated(false);
        barChart5.setOnMouseClicked(event -> barChart5.requestFocus());
        barChart5.addChartPlugins(FXCollections.observableList(List.of(Plugins.all())));


        chartPane1.getChildren().add(chartContainer1);
        chartContainer1.setPluggable((Pluggable) lineChart1);       

        chartPane2.getChildren().add(chartContainer2);
        chartContainer2.setPluggable((Pluggable) lineChart2);
        
        chartPane3.getChildren().add(chartContainer3);
        chartContainer3.setPluggable((Pluggable) lineChart3);
        
        chartContainer1b.setPluggable((Pluggable) barChart1);
        chartContainer2b.setPluggable((Pluggable) barChart2);
        chartContainer4.setPluggable((Pluggable) barChart4);
        chartContainer5.setPluggable((Pluggable) barChart5);
        
        ObservableList<XYChart.Series<Number, Number>> chartDataX = FXCollections.observableArrayList();
        ObservableList<XYChart.Series<Number, Number>> chartDataY = FXCollections.observableArrayList();
        ObservableList<XYChart.Series<Number, Number>> chartDataXBar = FXCollections.observableArrayList();
        ObservableList<XYChart.Series<Number, Number>> chartDataYBar = FXCollections.observableArrayList();
        ObservableList<XYChart.Series<Number, Number>> chartDataAmpl = FXCollections.observableArrayList();
        ObservableList<XYChart.Series<Number, Number>> chartDataCorrectorH = FXCollections.observableArrayList();
        ObservableList<XYChart.Series<Number, Number>> chartDataCorrectorV = FXCollections.observableArrayList();

        chartDataX.add(trajXSeries);
        chartDataY.add(trajYSeries);
        chartDataX.add(refXSeries);
        chartDataY.add(refYSeries);
        chartDataXBar.add(trajXSeriesBar);
        chartDataYBar.add(trajYSeriesBar);
        chartDataAmpl.add(AmplSeries);        
        chartDataAmpl.add(refAmplSeries);        
        chartDataCorrectorV.add(CVSeries);
        chartDataCorrectorH.add(CHSeries);
                
        lineChart1.setData(chartDataX);
        lineChart1.setSeriesColor(trajXSeries, colorX);   
        lineChart1.setSeriesColor(refXSeries, colorXref);
        lineChart1.setShowMarkers(true);
        lineChart1.setAnimated(false);
        lineChart1.setAxisSortingPolicy(LineChartFX.SortingPolicy.NONE);        
        
        lineChart2.setData(chartDataY);
        lineChart2.setSeriesColor(trajYSeries, colorY);
        lineChart2.setSeriesColor(refYSeries, colorYref);
        lineChart2.setShowMarkers(true);
        lineChart2.setAnimated(false);
        lineChart2.setAxisSortingPolicy(LineChartFX.SortingPolicy.NONE);

        lineChart3.setData(chartDataAmpl);
        lineChart3.setSeriesColor(AmplSeries, colorAmpl);
        lineChart3.setSeriesColor(refAmplSeries, colorAmplref);
        lineChart3.setAnimated(false);
        lineChart3.setShowMarkers(true);
        lineChart3.setAxisSortingPolicy(LineChartFX.SortingPolicy.NONE);
        
        barChart1.setData(chartDataXBar);    
        barChart1.setCreateSymbols(false);
        barChart1.setAnimated(false);
        barChart1.setHorizontalGridLinesVisible(false);
        barChart1.setVerticalGridLinesVisible(true);
        barChart1.setLegendVisible(false);
        //barChart1.setSeriesColor(trajXSeriesBar, colorX);
        barChart1.getStylesheets().add("styles/TrajX.css");        
        
        barChart2.setData(chartDataYBar);
        barChart2.setCreateSymbols(false);
        barChart2.setAnimated(false);
        barChart2.setHorizontalGridLinesVisible(false);
        barChart2.setVerticalGridLinesVisible(true);
        barChart2.setLegendVisible(false);
        //barChart2.setSeriesColor(trajYSeriesBar, colorY);
        barChart2.getStylesheets().add("styles/TrajY.css");
        
        barChart4.setData(chartDataCorrectorH);    
        barChart4.setCreateSymbols(false);
        barChart4.setAnimated(false);
        barChart4.setHorizontalGridLinesVisible(false);
        barChart4.setVerticalGridLinesVisible(true);
        barChart4.setLegendVisible(false);
        //barChart4.setSeriesColor(CHSeries, colorXref);
        barChart4.getStylesheets().add("styles/ChartCH.css");                
        
        barChart5.setData(chartDataCorrectorV);
        barChart5.setCreateSymbols(false);
        barChart5.setAnimated(false);
        barChart5.setHorizontalGridLinesVisible(false);
        barChart5.setVerticalGridLinesVisible(true);
        barChart5.setLegendVisible(false);
        //barChart4.setSeriesColor(CVSeries, colorYref);
        barChart5.getStylesheets().add("styles/ChartCV.css");
        
        lineChart1.setSeriesDrawn(refXSeries.getName(), false);
        lineChart2.setSeriesDrawn(refYSeries.getName(), false);
        lineChart3.setSeriesDrawn(refAmplSeries.getName(), false);
        
        MainFunctions.mainDocument.getAcceleratorProperty().addChangeListener((ChangeListener) (ObservableValue o, Object oldVal, Object newVal) -> {
           
            //stop monitor           
            monitor.stop();                   
            
            //create an instance of the monitor for the channels
            monitor = new LiveChannelMonitor(DisplayTraj, DisplayCorr,updatePlot);
            
            //stop live trajectory updates
            MainFunctions.mainDocument.liveTrajectory.set(false);
            buttonPauseTrajectoryLive.setText("START");
            
            //Initialize arrays
            DisplayTraj.initBPMs(MainFunctions.mainDocument.getAccelerator());
            DisplayCorr.initCorrectors(MainFunctions.mainDocument.getAccelerator());
            ReferenceTraj.initBPMs(MainFunctions.mainDocument.getAccelerator()); 
            
            //Plot zeros
            updateDataset(DisplayTraj.getBPMs(MainFunctions.mainDocument.getAccelerator()),DisplayCorr.getCVs(MainFunctions.mainDocument.getAccelerator()),DisplayCorr.getCHs(MainFunctions.mainDocument.getAccelerator()));            
                        
            HashMap<Channel,BPM> inputChannelsX0;
            HashMap<Channel,BPM> inputChannelsY0;
            HashMap<Channel,BPM> inputChannelsAmpl0;
            HashMap<Channel,DipoleCorr> inputChannelsCV0;
            HashMap<Channel,DipoleCorr> inputChannelsCH0;
        
            inputChannelsX0 = DisplayTraj.getBPMChannelsX(MainFunctions.mainDocument.getAccelerator());
            inputChannelsY0 = DisplayTraj.getBPMChannelsY(MainFunctions.mainDocument.getAccelerator());
            inputChannelsAmpl0 = DisplayTraj.getBPMChannelsAmpl(MainFunctions.mainDocument.getAccelerator());
            inputChannelsCV0 = DisplayCorr.getCVChannels(MainFunctions.mainDocument.getAccelerator());
            inputChannelsCH0 = DisplayCorr.getCHChannels(MainFunctions.mainDocument.getAccelerator());
            
            //update channels in the monitor
            monitor.updatechannels(inputChannelsX0, inputChannelsY0, inputChannelsAmpl0, inputChannelsCV0, inputChannelsCH0);     
                
        });

        MainFunctions.mainDocument.getSequenceProperty().addListener((ChangeListener) (ObservableValue o, Object oldVal, Object newVal) -> {
           
            if (MainFunctions.mainDocument.getSequence() != null && newVal != oldVal){
                String getSeqName = MainFunctions.mainDocument.getSequence();
                
                lineChart1.removeAllMarkers();
                lineChart2.removeAllMarkers();
                lineChart3.removeAllMarkers();
                barChart1.removeAllMarkers();
                barChart2.removeAllMarkers();
                barChart4.removeAllMarkers();
                barChart5.removeAllMarkers();
         
                if (MainFunctions.mainDocument.displayTrajectoryFile.getValue() !=  null) {
                     if (MainFunctions.mainDocument.getAccelerator().getSequence(getSeqName) != null) {
                        updateDataset(DisplayTraj.getBPMs(MainFunctions.mainDocument.getAccelerator().getSequence(getSeqName)),DisplayCorr.getCVs(MainFunctions.mainDocument.getAccelerator().getSequence(getSeqName)),
                            DisplayCorr.getCHs(MainFunctions.mainDocument.getAccelerator().getSequence(getSeqName)));
                    } else if (MainFunctions.mainDocument.getAccelerator().getComboSequence(getSeqName) != null) {
                        updateDataset(DisplayTraj.getBPMs(MainFunctions.mainDocument.getAccelerator().getComboSequence(getSeqName)),DisplayCorr.getCVs(MainFunctions.mainDocument.getAccelerator().getComboSequence(getSeqName)),
                            DisplayCorr.getCHs(MainFunctions.mainDocument.getAccelerator().getComboSequence(getSeqName)));
                    }
                }           

                double pos_start = 0.0;
                double pos_end = 10.0;


                if (MainFunctions.mainDocument.getAccelerator().getSequence(getSeqName) != null) {
                    pos_start = MainFunctions.mainDocument.getAccelerator().getSequence(getSeqName).getPosition();
                    pos_end = MainFunctions.mainDocument.getAccelerator().getSequence(getSeqName).getLength()+pos_start;
                } else if (MainFunctions.mainDocument.getAccelerator().getComboSequence(getSeqName) != null) {
                    pos_start = MainFunctions.mainDocument.getAccelerator().getComboSequence(getSeqName).getConstituents().get(0).getPosition();
                    pos_end = MainFunctions.mainDocument.getAccelerator().getComboSequence(getSeqName).getLength()+pos_start;
                    
                    // Add Vertical range marker for different sequences
                    int j = 1;
                    for (AcceleratorSeq seq : MainFunctions.mainDocument.getAccelerator().getComboSequence(getSeqName).getConstituents()){
                        if(!"ISRC".equals(seq.getId()) && !"LEBT".equals(seq.getId()) && !"RFQ".equals(seq.getId())){
                            if(j%2 != 0){                                 
                                lineChart1.addVerticalRangeMarker(new Data<>(seq.getPosition(), seq.getPosition()+seq.getLength()));
                                lineChart2.addVerticalRangeMarker(new Data<>(seq.getPosition(), seq.getPosition()+seq.getLength()));
                                lineChart3.addVerticalRangeMarker(new Data<>(seq.getPosition(), seq.getPosition()+seq.getLength()));
                                barChart1.addVerticalRangeMarker(new Data<>(seq.getPosition(), seq.getPosition()+seq.getLength()));
                                barChart2.addVerticalRangeMarker(new Data<>(seq.getPosition(), seq.getPosition()+seq.getLength()));
                                barChart4.addVerticalRangeMarker(new Data<>(seq.getPosition(), seq.getPosition()+seq.getLength()));
                                barChart5.addVerticalRangeMarker(new Data<>(seq.getPosition(), seq.getPosition()+seq.getLength()));
                            }
                            j = j+1;
                        }
                    }
                }
                //set position xmax and xmin in the plots
                lineChart1.getXAxis().setAutoRanging(false);
                xAxis1.setLowerBound(pos_start);
                xAxis1.setUpperBound(pos_end);
                
                lineChart2.getXAxis().setAutoRanging(false);
                xAxis2.setLowerBound(pos_start);
                xAxis2.setUpperBound(pos_end);
                
                lineChart3.getXAxis().setAutoRanging(false);
                xAxis3.setLowerBound(pos_start);
                xAxis3.setUpperBound(pos_end);
                
                barChart1.getXAxis().setAutoRanging(false);
                xAxis1b.setLowerBound(pos_start);
                xAxis1b.setUpperBound(pos_end);
                
                barChart2.getXAxis().setAutoRanging(false);
                xAxis2b.setLowerBound(pos_start);
                xAxis2b.setUpperBound(pos_end);
                
                barChart4.getXAxis().setAutoRanging(false);
                xAxis4.setLowerBound(pos_start);
                xAxis4.setUpperBound(pos_end);
                
                barChart5.getXAxis().setAutoRanging(false);
                xAxis5.setLowerBound(pos_start);
                xAxis5.setUpperBound(pos_end);                                

            }                       
        });


        //TrajectoryMenu
        MainFunctions.mainDocument.saveTrajectoryFile.addListener((ChangeListener) (ObservableValue o, Object oldVal, Object newVal) -> {

            URL urlselectedfile = null;
            File selectedFile = new File(MainFunctions.mainDocument.saveTrajectoryFile.getValue());

            try {
                urlselectedfile = selectedFile.toURI().toURL();
            } catch (MalformedURLException ex) {
                Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (urlselectedfile != null) {
                try {
                    //Save trajectory for the whole machine
                    DisplayTraj.saveTrajectory(MainFunctions.mainDocument.getAccelerator(), urlselectedfile);
                } catch (ConnectionException | GetException ex) {
                    Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
                }
                //Add saved trajectory to the ref traj list
                comboBoxRefTrajectory.getItems().add(urlselectedfile.toString());
            }

        });

        MainFunctions.mainDocument.refTrajectoryFile.addListener((ChangeListener) (ObservableValue o, Object oldVal, Object newVal) -> {

            File selectedFile = new File(MainFunctions.mainDocument.refTrajectoryFile.getValue());
            Accelerator accelerator = MainFunctions.mainDocument.getAccelerator();
            
            if (selectedFile.exists()) {
                comboBoxRefTrajectory.getItems().add(selectedFile.toString());
                comboBoxRefTrajectory.setValue(selectedFile.toString());
                
                try {
                        ReferenceTraj.loadTrajectory(accelerator, selectedFile);
                        DisplayTraj.loadReferenceTrajectory(comboBoxRefTrajectory.getSelectionModel().getSelectedItem());
                    } catch (IOException ex) {
                        Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
                    }
            }

        });

        MainFunctions.mainDocument.displayTrajectoryFile.addListener((ChangeListener) (ObservableValue o, Object oldVal, Object newVal) -> {
            
            if(MainFunctions.mainDocument.liveTrajectory.get()){
                
                buttonPauseTrajectoryLive.fire();
            
                if (MainFunctions.mainDocument.displayTrajectoryFile.getValue() != null ){
                    File selectedFile = new File(MainFunctions.mainDocument.displayTrajectoryFile.getValue());

                    String getSeqName = MainFunctions.mainDocument.getSequence();
                    Accelerator accelerator = MainFunctions.mainDocument.getAccelerator();

                    if (selectedFile.exists()) {
                        try {
                            DisplayTraj.loadTrajectory(accelerator, selectedFile);
                        } catch (IOException ex) {
                            Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
                        }

                        if (MainFunctions.mainDocument.getAccelerator().getSequence(getSeqName) != null && !MainFunctions.mainDocument.liveTrajectory.get()) {
                            updateDataset(DisplayTraj.getBPMs(MainFunctions.mainDocument.getAccelerator().getSequence(getSeqName)),DisplayCorr.getCVs(MainFunctions.mainDocument.getAccelerator().getSequence(getSeqName)),
                                DisplayCorr.getCHs(MainFunctions.mainDocument.getAccelerator().getSequence(getSeqName)));
                        } else if (MainFunctions.mainDocument.getAccelerator().getComboSequence(getSeqName) != null && !MainFunctions.mainDocument.liveTrajectory.get()) {
                            updateDataset(DisplayTraj.getBPMs(MainFunctions.mainDocument.getAccelerator().getComboSequence(getSeqName)),DisplayCorr.getCVs(MainFunctions.mainDocument.getAccelerator().getComboSequence(getSeqName)),
                                DisplayCorr.getCHs(MainFunctions.mainDocument.getAccelerator().getComboSequence(getSeqName)));
                        }

                    }                       

                    MainFunctions.mainDocument.displayTrajectoryFile.setValue(null);
                }
            }

        });        

        MainFunctions.mainDocument.parameterTrajPlot.addListener((ChangeListener) (ObservableValue o, Object oldVal, Object newVal) -> {
            if (MainFunctions.mainDocument.parameterTrajPlot.getValue().contains("Hor.+Ver. (Line)")){                       
                chartPane1.getChildren().clear();
                chartPane1.getChildren().add(chartContainer1);
                chartPane2.getChildren().clear();
                chartPane2.getChildren().add(chartContainer2);
            } else if (MainFunctions.mainDocument.parameterTrajPlot.getValue().contains("Hor.+Ver. (Bar)")){
                chartPane1.getChildren().clear();
                chartPane1.getChildren().add(chartContainer1b);
                chartPane2.getChildren().clear();
                chartPane2.getChildren().add(chartContainer2b);              
            } else if (MainFunctions.mainDocument.parameterTrajPlot.getValue().contains("Hor. (Line) + HC")){
                chartPane1.getChildren().clear();
                chartPane1.getChildren().add(chartContainer1);
                chartPane2.getChildren().clear();
                chartPane2.getChildren().add(chartContainer4);      
            } else if (MainFunctions.mainDocument.parameterTrajPlot.getValue().contains("Hor. (Bar) + HC")){
                chartPane1.getChildren().clear();
                chartPane1.getChildren().add(chartContainer1b);
                chartPane2.getChildren().clear();
                chartPane2.getChildren().add(chartContainer4); 
            } else if (MainFunctions.mainDocument.parameterTrajPlot.getValue().contains("Ver (Line) + VC")){
                chartPane1.getChildren().clear();
                chartPane1.getChildren().add(chartContainer2);
                chartPane2.getChildren().clear();
                chartPane2.getChildren().add(chartContainer5);    
            } else if (MainFunctions.mainDocument.parameterTrajPlot.getValue().contains("Ver (Bar) + VC")){
                chartPane1.getChildren().clear();
                chartPane1.getChildren().add(chartContainer2b);
                chartPane2.getChildren().clear();
                chartPane2.getChildren().add(chartContainer5);    
            } else {
                chartPane1.getChildren().clear();
                chartPane1.getChildren().add(chartContainer1);
                chartPane2.getChildren().clear();
                chartPane2.getChildren().add(chartContainer2);                         
            }           
        });

        comboPersistence.getSelectionModel().selectedItemProperty().addListener( (ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (oldValue != newValue){                
                double val = 1000/comboPersistence.getValue();                
                monitor.setSyncTime((int) val);               
            }
            
        });
    }

    public void updateDataset(List<BPM> BPMList, List<DipoleCorr> CVList, List<DipoleCorr> CHList){

        String nameHor = "x_rms = " + String.format("%.3f", DisplayTraj.getXrms(BPMList)) + " um";
        String nameVer = "y_rms = " + String.format("%.3f", DisplayTraj.getYrms(BPMList)) + " um";

        //Initializes the chart and chartData
        labelXrms.setText(nameHor);
        labelYrms.setText(nameVer);

        ObservableList<XYChart.Data<Number, Number>> refXData = FXCollections.observableArrayList();
        ObservableList<XYChart.Data<Number, Number>> refYData = FXCollections.observableArrayList();
        ObservableList<XYChart.Data<Number, Number>> refAmplData = FXCollections.observableArrayList();
        ObservableList<XYChart.Data<Number, Number>> XData = FXCollections.observableArrayList();
        ObservableList<XYChart.Data<Number, Number>> YData = FXCollections.observableArrayList();
        ObservableList<XYChart.Data<Number, Number>> XDataBar = FXCollections.observableArrayList();
        ObservableList<XYChart.Data<Number, Number>> YDataBar = FXCollections.observableArrayList();
        ObservableList<XYChart.Data<Number, Number>> AmplData = FXCollections.observableArrayList();
        ObservableList<XYChart.Data<Number, Number>> CHData = FXCollections.observableArrayList();
        ObservableList<XYChart.Data<Number, Number>> CVData = FXCollections.observableArrayList();

        //clear the initial values
        trajXSeries.getData().clear();
        trajYSeries.getData().clear();
        trajXSeriesBar.getData().clear();
        trajYSeriesBar.getData().clear();
        AmplSeries.getData().clear();
        refXSeries.getData().clear();
        refYSeries.getData().clear();
        CVSeries.getData().clear();
        CHSeries.getData().clear();
        
        double minDist = 0.25*DisplayTraj.getminDistanceBPMs(BPMList);

        for(BPM item : BPMList){

            //if(comboPersistence.getValue()>0){
            //    bufferXData.get(item).insert(DisplayTraj.X.get(item));
            //    //bufferXData.get(item).removeOldest();
            //    bufferYData.get(item).insert(DisplayTraj.Y.get(item));
            //    //bufferYData.get(item).removeOldest();
            //    bufferAmplData.get(item).insert(DisplayTraj.AvgAmpl.get(item));
            //    //bufferAmplData.get(item).removeOldest();

            //    ReferenceTraj.setPositionX(item, bufferXData.get(item).getAverage());
            //    ReferenceTraj.setPositionY(item, bufferYData.get(item).getAverage());             
            //    ReferenceTraj.setAmplitude(item, bufferAmplData.get(item).getAverage());

            //}

            XData.add(new XYChart.Data<>(DisplayTraj.Pos.get(item), DisplayTraj.XDiff.get(item)));
            YData.add(new XYChart.Data<>(DisplayTraj.Pos.get(item), DisplayTraj.YDiff.get(item)));
            //refXData.add(new XYChart.Data<>(ReferenceTraj.Pos.get(item), ReferenceTraj.XDiff.get(item)));
            //refYData.add(new XYChart.Data<>(ReferenceTraj.Pos.get(item), ReferenceTraj.YDiff.get(item)));
            refXData.add(new XYChart.Data<>(DisplayTraj.Pos.get(item), DisplayTraj.XRef.get(item)));
            refYData.add(new XYChart.Data<>(DisplayTraj.Pos.get(item), DisplayTraj.YRef.get(item)));
            AmplData.add(new XYChart.Data<>(DisplayTraj.Pos.get(item), DisplayTraj.AvgAmpl.get(item)));
            refAmplData.add(new XYChart.Data<>(ReferenceTraj.Pos.get(item), ReferenceTraj.AvgAmpl.get(item)));            
            
            XDataBar.add(new XYChart.Data<>(DisplayTraj.Pos.get(item)-minDist, 0.0));            
            XDataBar.add(new XYChart.Data<>(DisplayTraj.Pos.get(item)-minDist, DisplayTraj.XDiff.get(item)));            
            XDataBar.add(new XYChart.Data<>(DisplayTraj.Pos.get(item)+minDist, DisplayTraj.XDiff.get(item)));            
            XDataBar.add(new XYChart.Data<>(DisplayTraj.Pos.get(item)+minDist, 0.0));            
                        
            YDataBar.add(new XYChart.Data<>(DisplayTraj.Pos.get(item)-minDist, 0.0));            
            YDataBar.add(new XYChart.Data<>(DisplayTraj.Pos.get(item)-minDist, DisplayTraj.YDiff.get(item)));            
            YDataBar.add(new XYChart.Data<>(DisplayTraj.Pos.get(item)+minDist, DisplayTraj.YDiff.get(item)));            
            YDataBar.add(new XYChart.Data<>(DisplayTraj.Pos.get(item)+minDist, 0.0));     
        }
        
        
        minDist = 0.25*DisplayCorr.getminDistanceCVs(CVList);
        
        for(DipoleCorr item : CVList){
            CVData.add(new XYChart.Data<>(DisplayCorr.PosV.get(item)-minDist, 0.0));            
            CVData.add(new XYChart.Data<>(DisplayCorr.PosV.get(item)-minDist, DisplayCorr.CV.get(item)));            
            CVData.add(new XYChart.Data<>(DisplayCorr.PosV.get(item)+minDist, DisplayCorr.CV.get(item)));            
            CVData.add(new XYChart.Data<>(DisplayCorr.PosV.get(item)+minDist, 0.0));            
        }
        
        minDist = 0.25*DisplayCorr.getminDistanceCHs(CHList);
        
        for(DipoleCorr item : CHList){
            CHData.add(new XYChart.Data<>(DisplayCorr.PosH.get(item)-minDist, 0.0));            
            CHData.add(new XYChart.Data<>(DisplayCorr.PosH.get(item)-minDist, DisplayCorr.CH.get(item)));            
            CHData.add(new XYChart.Data<>(DisplayCorr.PosH.get(item)+minDist, DisplayCorr.CH.get(item)));            
            CHData.add(new XYChart.Data<>(DisplayCorr.PosH.get(item)+minDist, 0.0));            
        }
     
        //update plot data
        trajXSeries.setData(XData);
        trajYSeries.setData(YData);
        trajXSeriesBar.setData(XDataBar);
        trajYSeriesBar.setData(YDataBar);
        AmplSeries.setData(AmplData);
        refXSeries.setData(refXData);
        refYSeries.setData(refYData);
        refAmplSeries.setData(refAmplData);
        CVSeries.setData(CVData);
        CHSeries.setData(CHData);
       
    }
   
    @FXML
    private void handleChooseRefTrajectory(ActionEvent event) {
        try {
            DisplayTraj.loadReferenceTrajectory(comboBoxRefTrajectory.getSelectionModel().getSelectedItem());
            ReferenceTraj.loadReferenceTrajectory(comboBoxRefTrajectory.getSelectionModel().getSelectedItem());
        } catch (IOException ex) {
            Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }   

    @FXML
    private void buttonPauseTrajectoryLive(ActionEvent event) {
        
        boolean aux = MainFunctions.mainDocument.liveTrajectory.get();
        
        MainFunctions.mainDocument.liveTrajectory.set(!aux);
                
        if (MainFunctions.mainDocument.getSequence() != null && MainFunctions.mainDocument.liveTrajectory.get()) {
            if(!monitor.isAlive()){
                monitor.start();
            }
            buttonPauseTrajectoryLive.setText("PAUSE");
        } else {
            buttonPauseTrajectoryLive.setText("START");
        }       
    }

    @FXML
    private void removeLinePlots(ActionEvent event) {
        if (noLine.isSelected()) {
            lineChart1.setShowLine(trajXSeries, false);
            lineChart1.setShowLine(refXSeries,false);
            lineChart2.setShowLine(trajYSeries, false);
            lineChart2.setShowLine(refYSeries,false);
        } else {
            lineChart1.setShowLine(trajXSeries, true);
            lineChart1.setShowLine(refXSeries,true);
            lineChart2.setShowLine(trajYSeries, true);
            lineChart2.setShowLine(refYSeries,true);
        }
    }


}
