/*
 * Copyright (C) 2018 European Spallation Source ERIC
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.trajectorydisplay;

import java.io.File;
import java.net.URL;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import xal.extension.fxapplication.XalFxDocument;

/**
 *
 * @author nataliamilas
 */
public class TrajectoryDisplayDocument extends XalFxDocument {

    public BooleanProperty liveTrajectory;

    public SimpleStringProperty refTrajectoryFile;

    public SimpleStringProperty displayTrajectoryFile;

    public SimpleStringProperty saveTrajectoryFile;

    public TrajectoryArray Trajectory;
    
    public SimpleStringProperty parameterTrajPlot;
    

    /* CONSTRUCTOR
     * in order to access to variable from the main class
     */
    public TrajectoryDisplayDocument(Stage stage) {
        super(stage);
        DEFAULT_FILENAME = "Trajectory.xml";
        WILDCARD_FILE_EXTENSION = "*.xml";
        HELP_PAGEID = "227688944";

        liveTrajectory = new SimpleBooleanProperty();
        refTrajectoryFile = new SimpleStringProperty();
        displayTrajectoryFile = new SimpleStringProperty();
        saveTrajectoryFile = new SimpleStringProperty();
        parameterTrajPlot = new SimpleStringProperty();

        liveTrajectory.setValue(false);
        refTrajectoryFile.setValue(null);
        displayTrajectoryFile.setValue(null);
        saveTrajectoryFile.setValue(null);
        parameterTrajPlot.setValue("Hor.+Ver. (Line)");
    }

    public void saveTrajectory() {

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save Trajectory File");

        //Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml");
        fileChooser.getExtensionFilters().add(extFilter);

        //Show save file dialog
        File selectedFile = fileChooser.showSaveDialog(null);

        saveTrajectoryFile.set(selectedFile.toString());

    }

    public void loadRefTrajectory() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Load Reference Trajectory File");

        //Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml");
        fileChooser.getExtensionFilters().add(extFilter);

        //Show save file dialog
        File selectedFile = fileChooser.showOpenDialog(null);

        refTrajectoryFile.set(selectedFile.toString());

    }

    public void getTrajectoryFromFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Load Trajectory File");

        //Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml");
        fileChooser.getExtensionFilters().add(extFilter);

        //Show save file dialog
        File selectedFile = fileChooser.showOpenDialog(null);

        if (selectedFile != null){
            displayTrajectoryFile.set(selectedFile.toString());
        }

    }

    @Override
    public void saveDocumentAs(URL url) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void loadDocument(URL url) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void newDocument() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
