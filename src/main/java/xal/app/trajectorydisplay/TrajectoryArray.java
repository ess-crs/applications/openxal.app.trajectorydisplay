/*
 * Copyright (C) 2018 European Spallation Source ERIC
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.trajectorydisplay;

/**
 * Defines a quantity to store on the entity Trajectory Array. Can store the
 * current orbit and the reference orbit at once. It is possible to reset the
 * values, read from file and also read from OpenXAL once can also define how
 * long is the array as you program also performs the calculation of the
 * difference orbit
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import xal.ca.BatchGetValueRequest;
import xal.ca.Channel;
import xal.ca.ChannelRecord;
import xal.ca.ConnectionException;
import xal.ca.GetException;
import xal.smf.Accelerator;
import xal.smf.AcceleratorSeq;
import xal.smf.AcceleratorSeqCombo;
import xal.smf.impl.BPM;
import xal.tools.data.DataAdaptor;
import xal.tools.xml.XmlDataAdaptor;

public class TrajectoryArray {

    /* ------------------------
  Class variables
 * ------------------------ */
    /**
     * Hashmaps with the BPM and position.
     *
     * @serial internal array storage.
     */
    HashMap<BPM, Double> Pos = new HashMap();
    HashMap<BPM, Double> X = new HashMap();
    HashMap<BPM, Double> Y = new HashMap();
    HashMap<BPM, Double> XDiff = new HashMap();
    HashMap<BPM, Double> YDiff = new HashMap();
    HashMap<BPM, Double> XRef = new HashMap();
    HashMap<BPM, Double> YRef = new HashMap();
    HashMap<BPM, Double> AvgAmpl = new HashMap();
    HashMap<BPM, Double> phase = new HashMap();
    HashMap<BPM, Double> phaseRef = new HashMap();
    HashMap<BPM, Double> phaseDiff = new HashMap();

    /**
     * Number of BPMs in the sequence.
     *
     * @serial number of BPMs.
     */
    protected int BPMnum;


    /* ------------------------
     Initialization
    * ------------------------ */
    /**
     * Initializes BPMs in the machine and try to connect
     *
     * @param accl accelerator.
     */
    public void initBPMs(Accelerator accl) {

        List<BPM> BPMList = accl.getAllNodesOfType("BPM");
        List<BPM> NPMList = accl.getAllNodesOfType("NPM");

        //remove all NPMs
        BPMList.remove(NPMList);
        
        X.clear();
        Y.clear();
        XRef.clear();
        YRef.clear();
        XDiff.clear();
        YDiff.clear();
        AvgAmpl.clear();
        phase.clear();
        phaseRef.clear();
        phaseDiff.clear();
        Pos.clear();
       

        //Fill the trajecotry arrays
        BPMList.forEach(bpm -> {
            X.put(bpm, 0.0);
            Y.put(bpm, 0.0);
            XRef.put(bpm, 0.0);
            YRef.put(bpm, 0.0);
            XDiff.put(bpm, 0.0);
            YDiff.put(bpm, 0.0);
            AvgAmpl.put(bpm, 0.0);
            phase.put(bpm, 0.0);
            phaseRef.put(bpm, 0.0);
            phaseDiff.put(bpm, 0.0);
            Pos.put(bpm, bpm.getSDisplay());
        });

        BPMnum = BPMList.size();

    }

    public void resetReference(){
        //Fill the trajecotry arrays
        Pos.keySet().forEach(bpm -> {
            XRef.put(bpm, 0.0);
            YRef.put(bpm, 0.0);
            phaseRef.put(bpm, 0.0);
            XDiff.put(bpm, X.get(bpm));
            YDiff.put(bpm, Y.get(bpm));
            phaseDiff.put(bpm, phase.get(bpm));
        });
    }

    /**
     * Resets the trajectory to zero
     */
    public void resetTrajectory() {

        Pos.keySet().stream().forEachOrdered((bpm) -> {
            X.put(bpm, 0.0);
            Y.put(bpm, 0.0);
            XDiff.put(bpm, -XRef.get(bpm));
            YDiff.put(bpm, -YRef.get(bpm));
            AvgAmpl.put(bpm, 0.0);
            phase.put(bpm, 0.0);
            phaseDiff.put(bpm, -phaseRef.get(bpm));
        });
    }

    /**
     * Calculates the horizontal rms spread of the trajectory
     *
     * @param BPMList list of BPMs to calculate rms value
     * @return rmsX value
     */
    public double getXrms(List<BPM> BPMList) {
        double rms = 0.0;
        for (BPM item : BPMList) {
            rms = rms + XDiff.get(item) * XDiff.get(item);
        }
        rms = Math.sqrt(1.0 / XDiff.size() * rms);

        return rms;
    }

    /**
     * Calculates the vertical rms spread of the trajectory
     *
     * @param BPMList list of BPMs to calculate rms value
     * @return rmsY value
     */
    public double getYrms(List<BPM> BPMList) {
        double rms = 0.0;
        for (BPM item : BPMList) {
            rms = rms + YDiff.get(item) * YDiff.get(item);
        }
        rms = Math.sqrt(1.0 / YDiff.size() * rms);

        return rms;

    }
    
     /**
     * Return the minimum distance between horizontal correctors
     *
     * @param BPMList list of BPMs.
     * @return list with all CVs in the sequence
     */
    public double getminDistanceBPMs(List<BPM> BPMList) {
                
        double ele1=BPMList.get(0).getSDisplay();
        double ele2;
        double minDist = ele1;
        
        for (int i = 1; i < BPMList.size(); i++) {      
            ele2 = BPMList.get(i).getSDisplay();
            if (ele2-ele1 < minDist ){
                minDist = ele2-ele1;
            }
            ele1 = ele2;
        }

        return minDist;
    }

    /**
     * Calculates the maximum value for a given set (absolute value)
     *
     * @param array Map with BPM nodes and values
     * @return double
     */
    public double getArraymax(HashMap<BPM, Double> array) {
        List<Double> arrayVal = new ArrayList<>();

        array.keySet().forEach(bpm -> arrayVal.add(array.get(bpm)));

        return arrayVal.stream().max(Comparator.comparing(i -> Math.abs(i))).orElse(0.0);
    }

     /**
     * Calculates the minimum value for a given set (absolute value)
     *
     * @param array Map with BPM nodes and values
     * @return double
     */
    public double getArraymin(HashMap<BPM, Double> array) {
        List<Double> arrayVal = new ArrayList<>();

        array.keySet().forEach(bpm -> arrayVal.add(array.get(bpm)));

        return arrayVal.stream().min(Comparator.comparing(i -> Math.abs(i))).orElse(0.0);
    }

    /**
     * Sets the number of BPMs in a sequence
     *
     * @param accl accelerator.
     * @param Seq sequence name.
     * @throws xal.ca.ConnectionException
     * @throws xal.ca.GetException
     */
    public void setBPMnum(Accelerator accl, String Seq) throws ConnectionException, GetException {
        BPMnum = accl.getSequence(Seq).getAllNodesOfType("BPM").size();
    }

    /**
     * Sets value of the horizontal reference trajectory at a BPM to a given
     * value
     *
     * @param bpm beam position monitor.
     * @param val trajectory value at the bpm.
     */
    public void setRefPositionX(BPM bpm, double val) {
        XRef.put(bpm, val);
    }

    /**
     * Sets value of the reference phase at a BPM to a given
     * value
     *
     * @param bpm beam position monitor.
     * @param val phase value at the bpm.
     */
    public void setRefPhase(BPM bpm, double val) {
        phaseRef.put(bpm, val);
    }

    /**
     * Sets value of the vertical reference trajectory at a BPM to a given value
     *
     * @param bpm beam position monitor.
     * @param val trajectory value at the bpm.
     */
    public void setRefPositionY(BPM bpm, double val) {
        YRef.put(bpm, val);
    }

    /**
     * Sets value of the horizontal trajectory at a BPM to a given
     * value
     *
     * @param bpm beam position monitor.
     * @param val trajectory value at the bpm.
     */
    public void setPositionX(BPM bpm, double val) {
        X.put(bpm, val);
        XDiff.put(bpm,val-XRef.get(bpm));
    }

    /**
     * Sets value of the vertical trajectory at a BPM to a given value
     *
     * @param bpm beam position monitor.
     * @param val trajectory value at the bpm.
     */
    public void setPositionY(BPM bpm, double val) {
        Y.put(bpm, val);
        YDiff.put(bpm,val-YRef.get(bpm));
    }

     /**
     * Sets value of the amplitude at a BPM to a given value
     *
     * @param bpm beam position monitor.
     * @param val trajectory value at the bpm.
     */
    public void setAmplitude(BPM bpm, double val) {
        AvgAmpl.put(bpm, val);
    }

    /**
     * Sets value of the phase at a BPM to a given
     * value
     *
     * @param bpm beam position monitor.
     * @param val phase value at the bpm.
     */
    public void setPhase(BPM bpm, double val) {
        phase.put(bpm, val);
        phaseDiff.put(bpm,val-phaseRef.get(bpm));
    }
    
    /**
     * Gets value of the horizontal trajectory at a BPM 
     *
     * @param bpm beam position monitor.
     * @return position value
     */
    public double getPositionX(BPM bpm) {
        return X.get(bpm);
        
    }

    /**
     * Gets value of the vertical trajectory at a BPM 
     *
     * @param bpm beam position monitor.
     * @return position value
     */
    public double getPositionY(BPM bpm) {
        return Y.get(bpm);
        
    }

     /**
     * Gets value of the amplitude at a BPM 
     *
     * @param bpm beam position monitor.
     * @return phase value
     */
    public double getAmplitude(BPM bpm) {
        return AvgAmpl.get(bpm);
    }

    /**
     * Gets value of the phase at a BPM
     *
     * @param bpm beam position monitor.     
     * @return phase value     
     */
    public double getPhase(BPM bpm) {
        return phase.get(bpm);        
    }


    /**
     * Lists channels related to horizontal trajectory
     *
     * @param Sequence accelerator sequence.
     * @return hashMap of input channels
     */
    public HashMap<Channel,BPM> getBPMChannelsX(AcceleratorSeq Sequence) {

        List<BPM> BPMList = Sequence.getAllNodesOfType("BPM");
        List<BPM> NPMList = Sequence.getAllNodesOfType("NPM");

        //remove all NPMs
        NPMList.forEach(npm->{
            BPMList.remove(npm);
        });

        HashMap<Channel,BPM> inputChannels = new HashMap();

        BPMList.forEach(bpm -> {
            inputChannels.put(bpm.getChannel(BPM.X_AVG_HANDLE),bpm);
        });

        return inputChannels;
    }

     /**
     * Lists channels related to vertical trajectory
     *
     * @param Sequence accelerator sequence.
     * @return hashMap of input channels
     */
    public HashMap<Channel,BPM> getBPMChannelsY(AcceleratorSeq Sequence) {

        List<BPM> BPMList = Sequence.getAllNodesOfType("BPM");
        List<BPM> NPMList = Sequence.getAllNodesOfType("NPM");

        //remove all NPMs
        NPMList.forEach(npm->{
            BPMList.remove(npm);
        });

        HashMap<Channel,BPM> inputChannels = new HashMap();

        BPMList.forEach(bpm -> {
            inputChannels.put(bpm.getChannel(BPM.Y_AVG_HANDLE),bpm);
        });

        return inputChannels;
    }

     /**
     * Lists channels related to BPM amplitude
     *
     * @param Sequence accelerator sequence.
     * @return hashMap of input channels
     */
    public HashMap<Channel,BPM> getBPMChannelsAmpl(AcceleratorSeq Sequence) {

        List<BPM> BPMList = Sequence.getAllNodesOfType("BPM");
        List<BPM> NPMList = Sequence.getAllNodesOfType("NPM");

        //remove all NPMs
        NPMList.forEach(npm->{
            BPMList.remove(npm);
        });

        HashMap<Channel,BPM> inputChannels = new HashMap();

        BPMList.forEach(bpm -> {
            inputChannels.put(bpm.getChannel(BPM.AMP_AVG_HANDLE),bpm);
        });

        return inputChannels;
    }

     /**
     * Lists channels related to BPM phase
     *
     * @param Sequence accelerator sequence.
     * @return hashMap of input channels
     */
    public HashMap<Channel,BPM> getBPMChannelsPhase(AcceleratorSeq Sequence) {

        List<BPM> BPMList = Sequence.getAllNodesOfType("BPM");
        List<BPM> NPMList = Sequence.getAllNodesOfType("NPM");

        //remove all NPMs
        NPMList.forEach(npm->{
            BPMList.remove(npm);
        });

        HashMap<Channel,BPM> inputChannels = new HashMap();

        BPMList.forEach(bpm -> {
            inputChannels.put(bpm.getChannel(BPM.PHASE_AVG_HANDLE),bpm);
        });

        return inputChannels;
    }


    /**
     * Tests and initializes horizontal BPMs in the given combo sequence
     *
     * @param ComboSequence accelerator sequence.
     * @return hashMap of input channels
     */
    public HashMap<Channel,BPM> getBPMChannelsX(AcceleratorSeqCombo ComboSequence) {

        List<BPM> BPMList = ComboSequence.getAllNodesOfType("BPM");
        List<BPM> NPMList = ComboSequence.getAllNodesOfType("NPM");

        //remove all NPMs
        NPMList.forEach(npm->{
            BPMList.remove(npm);
        });

        HashMap<Channel,BPM> inputChannels = new HashMap();

        BPMList.forEach(bpm -> {
            inputChannels.put(bpm.getChannel(BPM.X_AVG_HANDLE),bpm);
        });

        return inputChannels;
    }

    /**
     * Tests and initializes horizontal BPMs in the given combo sequence
     *
     * @param ComboSequence accelerator sequence.
     * @return hashMap of input channels
     */
    public HashMap<Channel,BPM> getBPMChannelsY(AcceleratorSeqCombo ComboSequence) {

        List<BPM> BPMList = ComboSequence.getAllNodesOfType("BPM");
        List<BPM> NPMList = ComboSequence.getAllNodesOfType("NPM");

        //remove all NPMs
        NPMList.forEach(npm->{
            BPMList.remove(npm);
        });

        HashMap<Channel,BPM> inputChannels = new HashMap();

        BPMList.forEach(bpm -> {
            inputChannels.put(bpm.getChannel(BPM.Y_AVG_HANDLE),bpm);
        });

        return inputChannels;
    }

    /**
     * Tests and initializes BPMs Amplitudes in the given combo sequence
     *
     * @param ComboSequence accelerator sequence.
     * @return hashMap of input channels
     */
    public HashMap<Channel,BPM> getBPMChannelsAmpl(AcceleratorSeqCombo ComboSequence) {

        List<BPM> BPMList = ComboSequence.getAllNodesOfType("BPM");
        List<BPM> NPMList = ComboSequence.getAllNodesOfType("NPM");

        //remove all NPMs
        NPMList.forEach(npm->{
            BPMList.remove(npm);
        });

        HashMap<Channel,BPM> inputChannels = new HashMap();

        BPMList.forEach(bpm -> {
            inputChannels.put(bpm.getChannel(BPM.AMP_AVG_HANDLE),bpm);
        });

        return inputChannels;
    }

     /**
     * Lists channels related to BPM phase
     *
     * @param ComboSequence accelerator sequence.
     * @return hashMap of input channels
     */
    public HashMap<Channel,BPM> getBPMChannelsPhase(AcceleratorSeqCombo ComboSequence) {

        List<BPM> BPMList = ComboSequence.getAllNodesOfType("BPM");
        List<BPM> NPMList = ComboSequence.getAllNodesOfType("NPM");

        //remove all NPMs
        NPMList.forEach(npm->{
            BPMList.remove(npm);
        });

        HashMap<Channel,BPM> inputChannels = new HashMap();

        BPMList.forEach(bpm -> {
            inputChannels.put(bpm.getChannel(BPM.PHASE_AVG_HANDLE),bpm);
        });

        return inputChannels;
    }

     /**
     *  Lists channels related to BPM horizontal trajectory
     *
     * @param acc accelerator
     * @return hashMap of input channels
     */
    public HashMap<Channel,BPM> getBPMChannelsX(Accelerator acc) {

        List<BPM> BPMList = acc.getAllNodesOfType("BPM");
        List<BPM> NPMList = acc.getAllNodesOfType("NPM");

        //remove all NPMs
        NPMList.forEach(npm->{
            BPMList.remove(npm);
        });

        HashMap<Channel,BPM> inputChannels = new HashMap();

        BPMList.forEach(bpm -> {
            inputChannels.put(bpm.getChannel(BPM.X_AVG_HANDLE),bpm);
        });

        return inputChannels;
    }

    /**
     *  Lists channels related to BPM vertical trajectory
     *
     * @param acc accelerator
     * @return hashMap of input channels
     */
    public HashMap<Channel,BPM> getBPMChannelsY(Accelerator acc) {

        List<BPM> BPMList = acc.getAllNodesOfType("BPM");
        List<BPM> NPMList = acc.getAllNodesOfType("NPM");

        //remove all NPMs
        NPMList.forEach(npm->{
            BPMList.remove(npm);
        });

        HashMap<Channel,BPM> inputChannels = new HashMap();

        BPMList.forEach(bpm -> {
            inputChannels.put(bpm.getChannel(BPM.Y_AVG_HANDLE),bpm);
        });

        return inputChannels;
    }

    /**
     *  Lists channels related to BPM amplitude
     *
     * @param acc accelerator
     * @return hashMap of input channels
     */
    public HashMap<Channel,BPM> getBPMChannelsAmpl(Accelerator acc) {

        List<BPM> BPMList = acc.getAllNodesOfType("BPM");
        List<BPM> NPMList = acc.getAllNodesOfType("NPM");

        //remove all NPMs
        NPMList.forEach(npm->{
            BPMList.remove(npm);
        });

        HashMap<Channel,BPM> inputChannels = new HashMap();

         BPMList.forEach(bpm -> {
            inputChannels.put(bpm.getChannel(BPM.AMP_AVG_HANDLE),bpm);
        });

        return inputChannels;
    }

    /**
     * Lists channels related to BPM phase
     *
     * @param acc accelerator
     * @return hashMap of input channels
     */
    public HashMap<Channel,BPM> getBPMChannelsPhase(Accelerator acc) {

        List<BPM> BPMList = acc.getAllNodesOfType("BPM");
        List<BPM> NPMList = acc.getAllNodesOfType("NPM");

        //remove all NPMs
        NPMList.forEach(npm->{
            BPMList.remove(npm);
        });

        HashMap<Channel,BPM> inputChannels = new HashMap();

        BPMList.forEach(bpm -> {
            inputChannels.put(bpm.getChannel(BPM.PHASE_AVG_HANDLE),bpm);
        });

        return inputChannels;
    }

    /**
     * List all BPMs
     *
     * @param Sequence accelerator sequence.
     * @return list with all BPMs in the sequence
     */
    public List<BPM> getBPMs(AcceleratorSeq Sequence) {

        List<BPM> BPMList = Sequence.getAllNodesOfType("BPM");
        List<BPM> NPMList = Sequence.getAllNodesOfType("NPM");

        //remove all NPMs
        NPMList.forEach(npm->{
            BPMList.remove(npm);
        });

        return BPMList;
    }

    /**
     * List all BPMs
     *
     * @param ComboSequence accelerator sequence.
     * @return list with all BPMs in the sequence
     */
    public List<BPM> getBPMs(AcceleratorSeqCombo ComboSequence) {

        List<BPM> BPMList = ComboSequence.getAllNodesOfType("BPM");
        List<BPM> NPMList = ComboSequence.getAllNodesOfType("NPM");

        //remove all NPMs
        NPMList.forEach(npm->{
            BPMList.remove(npm);
        });

        return BPMList;
    }

    /**
     * Refactored code from getTrajectory()
     *
     * @param channels channels to query and add
     * @param request the requests for these channels (ie updated values)
     * @param U the base curve
     * @param UDiff the diff curve
     * @param URef the reference curve
     */
    private void addEntriesToMaps(HashMap<Channel,BPM> channels,
            BatchGetValueRequest request,
            HashMap<BPM, Double> U,
            HashMap<BPM, Double> UDiff,
            HashMap<BPM, Double> URef ) {
        double tmpref;
        ChannelRecord tmprecord;
        for (Map.Entry<Channel,BPM> entry : channels.entrySet()) {
            tmpref = URef.get(entry.getValue());
            tmprecord = request.getRecord(entry.getKey());
            if (tmprecord != null) {
                U.put(entry.getValue(), tmprecord.doubleValue());
                UDiff.put(entry.getValue(), U.get(entry.getValue()) - tmpref);
            } else {
                U.put(entry.getValue(), 0.0);
                UDiff.put(entry.getValue(), - tmpref);
            }
        }

    }

    /**
     * Request trajectory values
     *
     * @param accl accelerator.
     * @throws xal.ca.ConnectionException
     * @throws xal.ca.GetException
     */
    public void getTrajectory(Accelerator accl) throws ConnectionException, GetException {

        HashMap<Channel,BPM> channels;
        double waitTime = 5.0; // wait up to 5 seconds for a response

        resetTrajectory();

        channels = this.getBPMChannelsX(accl);
        BatchGetValueRequest request = new BatchGetValueRequest(channels.keySet());
        request.submitAndWait(waitTime);

        addEntriesToMaps(channels, request, X, XDiff, XRef);

        channels = this.getBPMChannelsY(accl);
        request = new BatchGetValueRequest(channels.keySet());
        request.submitAndWait(waitTime);   // wait up to 5 seconds for a response

        addEntriesToMaps(channels, request, Y, YDiff, YRef);

        channels = this.getBPMChannelsAmpl(accl);
        request = new BatchGetValueRequest(channels.keySet());
        request.submitAndWait(waitTime);   // wait up to 5 seconds for a response

        for (Map.Entry<Channel,BPM> entry : channels.entrySet()) {
            if (request.getRecord(entry.getKey()) != null) {
                AvgAmpl.put(entry.getValue(), request.getRecord(entry.getKey()).doubleValue());
            } else {
                Y.put(entry.getValue(), 0.0);
            }
        }

        channels = this.getBPMChannelsPhase(accl);
        request = new BatchGetValueRequest(channels.keySet());
        request.submitAndWait(5.0);   // wait up to 5 seconds for a response

        addEntriesToMaps(channels, request, phase, phaseDiff, phaseRef);

    }

    /**
     * Refactored code from getReferenceTrajectory()
     *
     * @param channels channels to query and add
     * @param request the requests for these channels (ie updated values)
     * @param U the base curve
     * @param UDiff the diff curve
     * @param URef the reference curve
     */
    private void addEntriesToReferenceMaps(HashMap<Channel,BPM> channels,
            BatchGetValueRequest request,
            HashMap<BPM, Double> U,
            HashMap<BPM, Double> UDiff,
            HashMap<BPM, Double> URef) {
        double tmpbase;
        ChannelRecord tmprecord;
        for (Map.Entry<Channel,BPM> entry : channels.entrySet()) {
            tmprecord = request.getRecord(entry.getKey());
            tmpbase = U.get(entry.getValue());
            if (tmprecord != null) {
                URef.put(entry.getValue(), tmprecord.doubleValue());
                UDiff.put(entry.getValue(), tmpbase - URef.get(entry.getValue()));
            } else {
                URef.put(entry.getValue(), 0.0);
                UDiff.put(entry.getValue(), tmpbase);
            }
        }

    }

    /**
     * Set the current trajectory at a given sequence as reference
     *
     * @param accl accelerator.
     * @throws xal.ca.ConnectionException
     * @throws xal.ca.GetException
     */
    public void getReferenceTrajectory(Accelerator accl) throws ConnectionException, GetException {

        HashMap<Channel,BPM> channels;

        resetReference();

        channels = this.getBPMChannelsX(accl);
        BatchGetValueRequest request = new BatchGetValueRequest(channels.keySet());
        request.submitAndWait(5.0);   // wait up to 5 seconds for a response

        addEntriesToReferenceMaps(channels, request, X, XDiff, XRef);

        channels = this.getBPMChannelsY(accl);
        request = new BatchGetValueRequest(channels.keySet());
        request.submitAndWait(5.0);   // wait up to 5 seconds for a response

        addEntriesToReferenceMaps(channels, request, Y, YDiff, YRef);

        channels = this.getBPMChannelsPhase(accl);
        request = new BatchGetValueRequest(channels.keySet());
        request.submitAndWait(5.0);   // wait up to 5 seconds for a response

        addEntriesToReferenceMaps(channels, request, phase, phaseRef, phaseDiff);

    }

    /**
     * Load a trajectory from URL
     *
     * @param accl Accelerator object
     * @param filename filename name of the file (full path).
     * @throws java.io.FileNotFoundException
     */
    public void loadTrajectory(Accelerator accl, URL filename) throws FileNotFoundException, IOException {
        DataAdaptor readAdp;
        String[] bpmNames;
        String namesTemp;
        double[] posS;
        double[] posX;
        double[] posY;
        double[] phi;
        List<String> listBPMname = new ArrayList<>();

        resetTrajectory();

        readAdp = XmlDataAdaptor.adaptorForUrl(filename, false);
        DataAdaptor header = readAdp.childAdaptor("ReferenceTrajectory");
        DataAdaptor trajData = header.childAdaptor("TrajectoryData");
        DataAdaptor phaseData = header.childAdaptor("PhaseData");
        DataAdaptor BPMData = trajData.childAdaptor("BPM");
        namesTemp = BPMData.stringValue("data");
        namesTemp = namesTemp.substring(1, namesTemp.length()- 1);
        bpmNames = namesTemp.split(",");
        for (int k = 0; k < bpmNames.length; k += 1) {
            listBPMname.add(bpmNames[k].replace(" ",""));
        }
        DataAdaptor PosData = trajData.childAdaptor("Position");
        posS = PosData.doubleArray("data");
        DataAdaptor XData = trajData.childAdaptor("Horizontal");
        posX = XData.doubleArray("data");
        DataAdaptor YData = trajData.childAdaptor("Vertical");
        posY = YData.doubleArray("data");
        phi = phaseData.doubleArray("data");

        listBPMname.forEach(name ->{
                try {
                    BPM item = (BPM) accl.getNodeWithId(name);
                    Pos.put(item, posS[listBPMname.indexOf(item.toString())]);
                    X.put(item, posX[listBPMname.indexOf(item.toString())]);
                    Y.put(item, posY[listBPMname.indexOf(item.toString())]);
                    phase.put(item, phi[listBPMname.indexOf(item.toString())]);
                    XDiff.put(item, X.get(item)- XRef.get(item));
                    YDiff.put(item, Y.get(item)- YRef.get(item));
                    phaseDiff.put(item, phase.get(item)- phaseRef.get(item));
                    AvgAmpl.put(item, 0.0);
                } catch (Exception e) {
                    Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, name+" not found in the current lattice.");
                }
        });

    }

    /**
     * Loads a trajectory from file
     *
     * @param accl Accelerator object
     * @param filename filename name of the file (full path).
     * @throws java.io.FileNotFoundException
     */
    public void loadTrajectory(Accelerator accl, File filename) throws FileNotFoundException, IOException {
        DataAdaptor readAdp;
        String[] bpmNames;
        String namesTemp;
        double[] posS;
        double[] posX;
        double[] posY;
        double[] phi;
        List<String> listBPMname = new ArrayList<>();

        resetTrajectory();

        readAdp = XmlDataAdaptor.adaptorForFile(filename, false);
        DataAdaptor header = readAdp.childAdaptor("ReferenceTrajectory");
        DataAdaptor trajData = header.childAdaptor("TrajectoryData");
        DataAdaptor phaseData = header.childAdaptor("PhaseData");
        DataAdaptor BPMData = trajData.childAdaptor("BPM");
        namesTemp = BPMData.stringValue("data");
        namesTemp = namesTemp.substring(1, namesTemp.length()- 1);
        bpmNames = namesTemp.split(",");
        for (int k = 0; k < bpmNames.length; k += 1) {
            listBPMname.add(bpmNames[k].replace(" ",""));
        }
        DataAdaptor PosData = trajData.childAdaptor("Position");
        posS = PosData.doubleArray("data");
        DataAdaptor XData = trajData.childAdaptor("Horizontal");
        posX = XData.doubleArray("data");
        DataAdaptor YData = trajData.childAdaptor("Vertical");
        posY = YData.doubleArray("data");
        phi = phaseData.doubleArray("data");

        listBPMname.forEach(name ->{
                try {
                    BPM item = (BPM) accl.getNodeWithId(name);
                    Pos.put(item, posS[listBPMname.indexOf(item.toString())]);
                    X.put(item, posX[listBPMname.indexOf(item.toString())]);
                    Y.put(item, posY[listBPMname.indexOf(item.toString())]);
                    phase.put(item, phi[listBPMname.indexOf(item.toString())]);
                    XDiff.put(item, X.get(item)- XRef.get(item));
                    YDiff.put(item, Y.get(item)- YRef.get(item));
                    phaseDiff.put(item, phase.get(item)- phaseRef.get(item));
                    AvgAmpl.put(item, 0.0);
                } catch (Exception e) {
                    Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, name+" not found in the current lattice.");
                }
        });

    }

    /**
     * Reads the reference trajectory from URL
     *
     * TODO Unused? Consider using URI instead of URL
     *
     * @param filename name of the file (full path).
     * @throws java.io.FileNotFoundException
     */
    public void loadReferenceTrajectory(URL filename) throws FileNotFoundException, IOException {
        DataAdaptor readAdp;
        String[] bpmNames;
        String namesTemp;
        double[] posX;
        double[] posY;
        double[] phi;
        List<String> listBPMname = new ArrayList<>();

        resetReference();

        if (filename.toString().equals("Zero Trajectory")){
            XRef.keySet().forEach(item -> {
                XRef.put(item,0.0);
                YRef.put(item,0.0);
                phaseRef.put(item,0.0);
                XDiff.put(item, X.get(item));
                YDiff.put(item, Y.get(item));
                phaseDiff.put(item, phase.get(item));
            });
        } else {
            readAdp = XmlDataAdaptor.adaptorForUrl(filename, false);
            DataAdaptor header = readAdp.childAdaptor("ReferenceTrajectory");
            DataAdaptor trajData = header.childAdaptor("TrajectoryData");
            DataAdaptor phaseData = header.childAdaptor("PhaseData");
            DataAdaptor BPMData = trajData.childAdaptor("BPM");
            namesTemp = BPMData.stringValue("data");
            namesTemp = namesTemp.substring(1, namesTemp.length()- 1);
            bpmNames = namesTemp.split(",");
            for (int k = 0; k < bpmNames.length; k += 1) {
                listBPMname.add(bpmNames[k].replace(" ",""));
            }
            DataAdaptor XData = trajData.childAdaptor("Horizontal");
            posX = XData.doubleArray("data");
            DataAdaptor YData = trajData.childAdaptor("Vertical");
            posY = YData.doubleArray("data");
            phi = phaseData.doubleArray("data");

            for (BPM item : XRef.keySet()){
                if (listBPMname.contains(item.getId())) {
                    XRef.put(item, posX[listBPMname.indexOf(item.getId())]);
                    YRef.put(item, posY[listBPMname.indexOf(item.getId())]);
                    phaseRef.put(item, phi[listBPMname.indexOf(item.toString())]);
                    XDiff.put(item, X.get(item) - posX[listBPMname.indexOf(item.getId())]);
                    YDiff.put(item, Y.get(item) -  posY[listBPMname.indexOf(item.getId())]);
                    phaseDiff.put(item, phase.get(item) - phi[listBPMname.indexOf(item.toString())]);
                }
            }
        }

    }

    /**
     * Reads the reference trajectory from file
     *
     * @param filename strong of the file (full path).
     * @throws java.io.FileNotFoundException
     */
    public void loadReferenceTrajectory(String filename) throws FileNotFoundException, IOException {
        DataAdaptor readAdp;
        String[] bpmNames;
        String namesTemp;
        double[] posX;
        double[] posY;
        double[] phi;
        List<String> listBPMname = new ArrayList<>();

        resetReference();

        if (filename.equals("Zero Trajectory")){
            XRef.keySet().forEach(item -> {
                XRef.put(item,0.0);
                YRef.put(item,0.0);
                phaseRef.put(item,0.0);
                XDiff.put(item, X.get(item));
                YDiff.put(item, Y.get(item));
                phaseDiff.put(item, phase.get(item));
            });
        } else {
            readAdp = XmlDataAdaptor.adaptorForUrl(filename, false);
            DataAdaptor header = readAdp.childAdaptor("ReferenceTrajectory");
            DataAdaptor trajData = header.childAdaptor("TrajectoryData");
            DataAdaptor phaseData = header.childAdaptor("PhaseData");
            DataAdaptor BPMData = trajData.childAdaptor("BPM");
            namesTemp = BPMData.stringValue("data");
            namesTemp = namesTemp.substring(1, namesTemp.length()- 1);
            bpmNames = namesTemp.split(",");
            for (int k = 0; k < bpmNames.length; k += 1) {
                listBPMname.add(bpmNames[k].replace(" ",""));
            }
            DataAdaptor XData = trajData.childAdaptor("Horizontal");
            posX = XData.doubleArray("data");
            DataAdaptor YData = trajData.childAdaptor("Vertical");
            posY = YData.doubleArray("data");
            phi = phaseData.doubleArray("data");


            for (BPM item : XRef.keySet()){
                if (listBPMname.contains(item.getId())) {
                    XRef.put(item, posX[listBPMname.indexOf(item.getId())]);
                    YRef.put(item, posY[listBPMname.indexOf(item.getId())]);
                    phaseRef.put(item, phi[listBPMname.indexOf(item.toString())]);
                    XDiff.put(item, X.get(item) - posX[listBPMname.indexOf(item.getId())]);
                    YDiff.put(item, Y.get(item) -  posY[listBPMname.indexOf(item.getId())]);
                    phaseDiff.put(item, phase.get(item) - phi[listBPMname.indexOf(item.toString())]);
                }
            }
        }

    }

    /**
     * Saves the trajectory to file
     *
     * @param accl accelerator object
     * @param filename name of the file (full path).
     * @throws xal.ca.ConnectionException
     * @throws xal.ca.GetException
     */
    public void saveTrajectory(Accelerator accl, URL filename) throws ConnectionException, GetException {
        //Saves the data into the file and set as reference
        XmlDataAdaptor da = XmlDataAdaptor.newEmptyDocumentAdaptor();
        DataAdaptor trajectoryAdaptor = da.createChild("ReferenceTrajectory");
        trajectoryAdaptor.setValue("title", filename.getPath());
        trajectoryAdaptor.setValue("date", new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date()));

        List<String> BPMnames = new ArrayList<>();
        double[] posS = new double[X.keySet().size()];
        double[] posX = new double[X.keySet().size()];
        double[] posY = new double[X.keySet().size()];
        double[] phi = new double[X.keySet().size()];

        getTrajectory(accl);


        int k = 0;
        for (Map.Entry<BPM, Double> entry : X.entrySet()) {
            BPMnames.add(entry.getKey().getId());
            posS[k] = Pos.get(entry.getKey());
            posX[k] = entry.getValue();
            posY[k] = Y.get(entry.getKey());
            phi[k] = phase.get(entry.getKey());
            k+=1;
        }

        DataAdaptor trajData = trajectoryAdaptor.createChild("TrajectoryData");
        trajData.setValue("Sequence",accl.getAllSeqs().toString());
        DataAdaptor BPMData = trajData.createChild("BPM");
        BPMData.setValue("data", BPMnames.toString());
        DataAdaptor PosData = trajData.createChild("Position");
        PosData.setValue("data", posS);
        DataAdaptor XData = trajData.createChild("Horizontal");
        XData.setValue("data", posX);
        DataAdaptor YData = trajData.createChild("Vertical");
        YData.setValue("data", posY);
        DataAdaptor phaseData = trajectoryAdaptor.createChild("PhaseData");
        phaseData.setValue("data", phi);



        da.writeToUrl(filename);

    }

}
